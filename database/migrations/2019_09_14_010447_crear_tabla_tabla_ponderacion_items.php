<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTablaPonderacionItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_ponderacion_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_item')->nullable();
            $table->text('nombre_item');
            $table->unsignedInteger('puntaje_maximo');
            $table->unsignedInteger('puntaje_minimo')->default(0);
            $table->unsignedBigInteger('tabla_ponderacion_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->enum('tipo', ['item', 'grupo', 'unico'])->default('item');
            $table->foreign('tabla_ponderacion_id')->references('id')->on('tabla_ponderacion')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parent_id')->references('id')->on('tabla_ponderacion_items')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuadro_ponderacion_items');
    }
}
