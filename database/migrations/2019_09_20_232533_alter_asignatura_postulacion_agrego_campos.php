<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAsignaturaPostulacionAgregoCampos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asignatura_postulacion', function (Blueprint $table) {
            $table->timestamp('editando')->nullable()
                ->comment("Esta campo guarda la hora en que esta siendo editado el puntaje de un postulante");
            $table->unsignedBigInteger('user_id')->nullable()
                ->comment("Esta campo guarda el usuario que edito el postulante editado el puntaje de un postulante");
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asignatura_postulacion', function (Blueprint $table) {
            $table->dropColumn('editando');
            $table->dropForeign('user_id');
            $table->dropColumn('user_id');
        });
    }
}
