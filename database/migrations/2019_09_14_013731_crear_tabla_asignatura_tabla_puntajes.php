<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAsignaturaTablaPuntajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignatura_tabla_puntajes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('puntaje');
            $table->unsignedBigInteger('asignatura_postulacion_id');
            $table->unsignedBigInteger('tabla_item_id');
            $table->unsignedBigInteger('tabla_ponderacion_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('asignatura_postulacion_id')->references('id')->on('asignatura_postulacion')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tabla_item_id')->references('id')->on('tabla_ponderacion_items')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tabla_ponderacion_id')->references('id')->on('tabla_ponderacion')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignatura_tabla_puntajes');
    }
}
