<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTablaPonderacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_ponderacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('letra')->unique();
            $table->string('nombre');
            $table->unsignedInteger('puntaje_maximo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_ponderacion');
    }
}
