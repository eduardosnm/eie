<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPostulaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('postulante_id');
            $table->unsignedBigInteger('llamado_id');
            $table->unsignedBigInteger('upload_id');
            $table->text('uuid')->nullable();
            $table->foreign('postulante_id')->references('id')->on('postulantes')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('llamado_id')->references('id')->on('llamados')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('upload_id')->references('id')->on('uploads')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulaciones');
    }
}
