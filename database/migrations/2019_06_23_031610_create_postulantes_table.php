<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostulantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('dni')->unique();
            $table->string('apellido');
            $table->string('nombre');
            $table->date('fecha_nacimiento');
            $table->string('lugar_nacimiento');
            $table->string('cuil_cuit');
            $table->string('domicilio');
            $table->string('telefono');
            $table->string('email');
            $table->text('titulos_grado');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulantes');
    }
}
