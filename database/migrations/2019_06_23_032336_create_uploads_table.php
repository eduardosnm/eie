<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('nombre');
            $table->string('alt')->nullable();
            $table->string('extension');
            $table->text('descripcion')->nullable();
            $table->string('mime');
            $table->unsignedBigInteger('tipo_upload_id');
            $table->morphs('uploadable');
            $table->foreign('tipo_upload_id')->references('id')->on('tipos_uploads')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
