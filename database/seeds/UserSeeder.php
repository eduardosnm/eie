<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(\App\User::class)->create([
            'nombre' => 'Mónica',
            'apellido' => 'Fernandez',
            'email' => 'informatica@eie.unse.edu.ar',
            'password' =>bcrypt('12345678'),
        ]);

        $admin->assign('admin');

        if (App::environment('local')){
            for ($i=0; $i<10; $i++){
                $user = factory(\App\User::class)->create(['password' => bcrypt('123456')]);
                $user->assign('evaluador');
            }
        }

        $admin2 = factory(\App\User::class)->create([
            'nombre' => 'Eduardo',
            'apellido' => 'Bravo',
            'email' => 'eduardosnm@msn.com',
            'password' =>bcrypt('12345678'),
        ]);

        $admin2->assign('admin');

    }
}
