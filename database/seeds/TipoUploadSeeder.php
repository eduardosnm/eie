<?php

use Illuminate\Database\Seeder;

class TipoUploadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = ['cv', 'tabla', 'reglamento'];
        for ($i=0; $i < count($tipos); $i++){
            factory(\App\TipoUpload::class)->create([
                'descripcion' => $tipos[$i],
            ]);
        }
    }
}
