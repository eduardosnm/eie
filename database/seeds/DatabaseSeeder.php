<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BouncerSeeder::class);
//        if (App::environment('local')){
//            $this->call(UserSeeder::class);
//            $this->call(CarreraSeeder::class);
//            $this->call(TipoUploadSeeder::class);
            $this->call(TablaPonderacionSeeder::class);
//        }
    }
}
