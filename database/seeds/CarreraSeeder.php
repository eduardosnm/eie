<?php

use Illuminate\Database\Seeder;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $asignaturas_lei= [
            "Primeras infancias, familias y educación: debates y problemáticas actuales",
            "La construcción de un curriculum integral para la primera infancia",
            "Pedagogías actuales en la educación infantil",
            "Territorios educativos para la primera infancia",
            "El juego, indagaciones y reflexiones teóricas en torno a su práctica",
            "El análisis de lo institucional: indagaciones e intervenciones",
            "Gestión, planeamiento y organización de instituciones educativas",
            "Perspectivas actuales de la evaluación educativa",
            "Taller de sistematización y escritura de experiencias",
            "Taller de diseño, análisis y evaluación de proyectos institucionales",
            "Psicomotricidad en la primera infancia",
            "Alfabetización inicial",
            "Los lenguajes del arte en la educación inicial",
            "La enseñanza mediada con TICs",
            "La atención a las trayectorias educativas",
            "Inclusión del niño con NEE",
            "Educación maternal",
            "Abordaje de la Educación sexual integral en la educación inicial",
            "Taller para la construcción del Trabajo Final"
        ];
        $lei = factory(\App\Carrera::class)->create([
            "nombre" => "Licenciatura en Educación Inicial",
            "acronimo" => "L.E.I."
        ]);

        foreach ($asignaturas_lei as $asignatura){
            factory(\App\Asignatura::class)->create([
                "nombre" => $asignatura,
                "carrera_id" => $lei->id
            ]);
        }

        $asignaturas_lge = [
            "Enfoque socio — cultural de la educación",
            "Gestión Educativa: perspectivas actuales",
            "Pensamiento Pedagógico Contemporáneo",
            "Gestión y Políticas Públicas de Educación",
            "Producción y análisis de información educativa",
            "Planeamiento Estratégico orientado a la Mejora",
            "Gestión de los procesos de Enseñanza y de Aprendizaje",
            "Optativa",
            "Práctica Profesional I",
            "Evaluación Institucional y toma de decisiones",
            "Optativa",
            "Práctica Profesional II",
            "Optativa",
        ];

        $lge = factory(\App\Carrera::class)->create([
            "nombre" => "Licenciatura en Gestión Educativa",
            "acronimo" => "L.G.E."
        ]);

        foreach ($asignaturas_lge as $asignatura){
            factory(\App\Asignatura::class)->create([
                "nombre" => $asignatura,
                "carrera_id" => $lge->id
            ]);
        }

        $asignaturas_lep = [
            "ENFOQUES PEDAGÓGICOS CONTEMPORÁNEO",
            "NUEVAS INFANCIAS FAMILIAS Y EDUCACION: DEBATES Y PROBLEMATICAS ACTUALES",
            "BASES NEUROCIENTIFICAS DEL APRENDIZAJE",
            "PROCESOS DE INCLUSIÓN EN LA EDUCACION PRIMARIA",
            "SISTEMATIZACIÓN DE PRACTICAS COMO HERRAMIENTA ANALITICA EN EDUCACIÓN",
            "CONTEXTOS ESCOLARES DIVERSOS SU ESPECIFICIDAD CURRICULAR",
            "NUEVOS ENFOQUES DEL DESARROLLO Y ANÁLISIS CURRICULAR",
            "PERSPECTIVAS ACTUALES DE LA EVALUACIÓN EDUCATIVA",
            "PRODUCCIÓN Y ANÁLISIS DE DATOS EDUCATIVOS INSTITUCIONALES",
            "ESTADÍSTICA EDUCATIVA",
            "DISEÑO ANÁLISIS Y EVALUACIÓN DE PROYECTOS INSTITUCIONALES",
            "ANÁLISIS DE POLITICAS EDUCATIVAS",
            "DISEÑO Y ANÁLISIS DE PRACTICAS PEDAGÓGICO DIDÁCTICAS",
            "ENSEÑANZA MEDIADA CON TECNOLOGIAS DE LA INFORMACIÓN Y DE LA COMUNICACIÓN",
            "PRÁCTICAS PROFESIONALES",
            "SEMINARIO DE FORMACIÓN ÉTICA Y CIUDADANA (OPTATIVA)",
            "SEMINARIO DE LENGUA Y LITERATURA (OPTATIVA)",
            "SEMINARIO DE MATEMÁTICA (OPTATIVA)",
            "EDUCACIÓN SEXUAL INTEGRAL EN LA EDUCACION PRIMARIA (OPTATIVA)",
            "SEMINARIO DE CIENCIAS NATURALES (OPTATIVA)",
            "SEMINARIO DE CIENCIAS SOCIALES(OPTATIVA)",
            "SEMINARIO DE EDUCACIÓN ARTÍSTICA (OPTATIVA)"
        ];

        $lep = factory(\App\Carrera::class)->create([
            "nombre" => "Licenciatura en Educación Primaria",
            "acronimo" => "L.E.P."
        ]);

        foreach ($asignaturas_lep as $asignatura){
            factory(\App\Asignatura::class)->create([
                "nombre" => ucfirst(mb_strtolower($asignatura)),
                "carrera_id" => $lep->id
            ]);
        }
    }
}
