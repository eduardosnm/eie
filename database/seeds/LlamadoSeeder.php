<?php

use Illuminate\Database\Seeder;

class LlamadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Llamado::class)->times(3)->create()
            ->each(function ($llamado){
               $llamado->asignaturas()->sync(\App\Asignatura::where('estado', true)->get()->random(5));
            });
    }
}
