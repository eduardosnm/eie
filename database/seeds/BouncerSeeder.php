<?php

use App\User;
use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->crearRoles();

        $this->createAbilities();

        Bouncer::allow('admin')->everything();
        Bouncer::allow('evaluador')->to('listado-postulantes');
        Bouncer::allow('evaluador')->to('ver-postulantes');
        Bouncer::allow('evaluador')->to('getCv-postulantes');
        Bouncer::allow('evaluador')->to('descargarCv-postulantes');
        Bouncer::allow('evaluador')->to('guardarPuntaje-postulantes');
        Bouncer::allow('evaluador')->to('evaluar-postulantes');
    }

    protected function crearRoles(): void
    {
        Bouncer::role()->create([
            'name' => 'admin',
            'title' => 'Administrador'
        ]);

        Bouncer::role()->create([
            'name' => 'evaluador',
            'title' => 'Tribunal'
        ]);
    }

    protected function createAbilities()
    {
        Bouncer::ability()->create([
            'name' => '*',
            'title' => 'Todas las habilidades',
            'entity_type' => '*',
        ]);

        Bouncer::ability()->createForModel(\App\User::class, [
            'name' => 'listado',
            'title' => 'Listar usuarios',
        ]);

        Bouncer::ability()->createForModel(\App\User::class, [
            'name' => 'crear',
            'title' => 'Crear usuario',
        ]);
    }
}
