<?php

use App\TablaPonderacion;
use App\TablaPonderacionItem;
use Illuminate\Database\Seeder;

class TablaPonderacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $docencia = [
            ["A1", "<p>TÍTULO DE BASE (Máxima pertinencia 60, otros de grado 40)</p>
            <p>Título de grado universitario para la convocatoria (60)</p>
            <p>Otros Título de grado (40)</p>",60
            ],
            ["A2", "<p>ANTIGÜEDAD DOCENTE EN EDUCACIÓN A DISTANCIA: 2 puntos por año (hasta 30)</p>
            <p>ANTIGÜEDAD DOCENTE EN EL SISTEMA EDUCATIVO FORMAL: 1 punto por año (hasta 20).</p>",30
            ],
            ["A3", "<p>INTEGRACIÓN DE EQUIPO DOCENTE DE GRADO</p>
            <p>Responsable del dictado de espacios curriculares en EaD 5 puntos cada uno (hasta 70)</p>",70
            ],
            ["A4","<p>DOCENCIA DE POSGRADO</p>
                    <p>Dictado de cursos de posgrado: 5 puntos cada curso posgrado (hasta 40)</p>
                    <p>Dictado de postítulos: 3 puntos por cada postítulo dictado (hasta 20)</p>",40],
            ["A5", "<p>PRODUCCIÓN DOCENTE</p>",150, 'grupo'],
            ["", "<p>Libro de la disciplina como autor: 20 puntos cada uno (hasta 50)</p>",50],
            ["", "<p>Libro como coautor o Capítulo de libro de la disciplina motivo de la convocatoria: 10 cada uno (hasta 40)</p>",40],
            ["", "<p>Autor de módulos y/o diseños curriculares para ser utilizados en la EaD: 10 puntos cada uno (hasta 40)</p>",40],
            ["", "<p>Autor de Programa/Proyecto aprobado y ejecutado en el nivel educativo objeto de la convocatoria y/o en EaD: 10 puntos cada uno (hasta 20).</p>
                <p>Integrante de equipo comisión de formulación de Programas y/o Proyectos Educativos: 5 puntos cada uno (hasta 20).</p>",20],
            ["A6", "<p>JURADO DE CONCURSO DOCENTE, INTEGRACIÓN DE COMISIONES EVALUADORAS EN EL ÁMBITO DOCENTE 10 puntos por cada comisión (hasta 40).</p>",40],
            ["A7", "<p>DIRECCIÓN, TUTORÍAS O PROFESOR GUÍA DE GRADO: TESIS, TESINAS, TRABAJOS FINALES, PASANTÍAS, PRÁCTICAS PROFESIONALES SUPERVISADAS, BECARIOS DE GRADO 5 puntos por cada uno (hasta 50).</p>",50],
            ["A8", "<p>INTEGRACIÓN DE JURADOS DE TRABAJOS FINALES, TESIS Y TESINAS, ETC. 5 puntos cada uno (hasta 30).</p>",30],
            ["A9", "<p>ORGANIZACIÓN DE EVENTOS ACADÉMICOS: INTEGRANTE DEL COMITE ORGANIZADOR DE JORNADAS, CONGRESOS, WORKSHOPS U OTROS. 5 puntos por cada evento (hasta 20)</p>",20],
            ["A10", "<p>FORMACIÓN DE POSTGRADO (computar <b>solo</b> el nivel máximo alcanzado)</p>",70, 'unico'],
            ["", "<p>Doctorado</p>",70, 'unico', 70],
            ["", "<p>Maestría</p>",60, 'unico', 60],
            ["", "<p>Especialista</p>",50, 'unico', 50],
            ["", "<p>Cursos de Posgrados de más de 40hs relacionados con el área disciplinar que se concursa (8 ptos. c/u). Hasta un máximo de 40.</p>",40, 'unico', 8],
            ["A11", "<p>OTRAS CAPACITACIONES (CURSOS, POSTÍTULO, CAPACITACIONES PEDAGÓGICAS, OTROS TÍTULOS DE GRADO)</p>
                    <p>Cursos de EaD de más de 40hs. 5 puntos cada uno (hasta un máximo de 20).</p>
                    <p>Cursos de EaD relacionados con el área curricular motivo de la convocatoria de menos de 40hs. 5 puntos cada.uno (hasta un máximo de 20)</p>",40],

        ];

        $doc = factory(TablaPonderacion::class)->create([
            "nombre" => "Docencia",
            "letra" => "A",
            "puntaje_maximo" => 600
        ]);

        $parent_id = null;
        foreach ($docencia as $item){

            $tabla_item = new TablaPonderacionItem();

            $tabla_item->codigo_item = $item[0];
            $tabla_item->nombre_item = $item[1];
            $tabla_item->puntaje_maximo = $item[2];
            $tabla_item->tabla_ponderacion_id = $doc->id;
            $tabla_item->tipo = isset($item[3]) ? $item[3] : 'item';
            $tabla_item->puntaje_minimo = isset($item[4]) ? $item[4] : 0;
            $tabla_item->save();

            if (!empty($item[0])){
                $parent_id = $tabla_item->id;
            }else{
                $tabla_item->parent_id =  $parent_id;
                $tabla_item->save();
            }

        }

        $investigaction = [
            ["B2", "<p>Director /Codirector o Integrantes de Programas y/o Proyectos de investigación relacionados con el área disciplinar objeto de la convocatoria y/o la EaD. 10 puntos cada uno (hasta un máximo de 30)</p>", 30],
            ["B3", "<p>Producción en investigación: Libros, capítulo de libros productos de la investigación y presentación en reuniones científicas o en revistas, relacionados con el área disciplinar y/o la EaD. 5 puntos por cada presentación EaD a congreso. 10 puntos para
            presentaciones en revistas. 20 puntos por cada libro.</p>", 60],
            ["B4", "<p>Dirección de tesis de posgrado o becarios de investigación (15 puntos por cada tesis; 10 por cada becario).</p>", 30],
            ["B5", "<p>Evaluación de proyectos, de pares evaluadores; o integrante de comités evaluadores. Jurado de tesis de Posgrado (5 puntos por cada una)</p>", 30],
        ];

        $inv = factory(TablaPonderacion::class)->create([
            "nombre" => "Actividades de investigación",
            "letra" => "B",
            "puntaje_maximo" => 150
        ]);
        foreach ($investigaction as $item){
            $tabla_item = new TablaPonderacionItem();
            $tabla_item->codigo_item = $item[0];
            $tabla_item->nombre_item = $item[1];
            $tabla_item->puntaje_maximo = $item[2];
            $tabla_item->tabla_ponderacion_id = $inv->id;
            $tabla_item->puntaje_minimo = isset($item[4]) ? $item[4] : 0;
            $tabla_item->save();
        }

        $extension = [
            ["C1", "<p>Director /Codirector o Integrante de Programas y/o Proyectos de extensión, vinculación y transferencia (10 puntos máximo por
                    cada participación)</p>", 30],
            ["C2", "<p>Evaluación de proyectos de extensión, vinculación y transferencia; de pares evaluadores, o integrante de comités evaluadores de extensión, vinculación y transferencia (5 puntos
                    por cada actividad).</p>", 20],
            ["C3", "<p>Producción y socialización de los resultados de las actividades de extensión, vinculación y transferencia realizadas en reuniones
                    académicas (5 puntos cada presentación y/o actividad)</p>", 30],
            ["C4", "<p>Publicación de artículos relacionados con la extensión, vinculación y transferencia realizadas y la EaD en revistas o en la Web (10 puntos por cada artículo publicado)</p>", 20],
            ["C5", "<p>Libros o capitulo de libros productos de las actividades de extensión realizadas (20 puntos cada libro publicado; 10 puntos cada capítulo de libro).</p>", 30],
        ];

        $ext = factory(TablaPonderacion::class)->create([
            "nombre" => "Extensión, Vinculación y Transferencia",
            "letra" => "C",
            "puntaje_maximo" => 130
        ]);
        foreach ($extension as $item){
            $tabla_item = new TablaPonderacionItem();
            $tabla_item->codigo_item = $item[0];
            $tabla_item->nombre_item = $item[1];
            $tabla_item->puntaje_maximo = $item[2];
            $tabla_item->tabla_ponderacion_id = $ext->id;
            $tabla_item->puntaje_minimo = isset($item[4]) ? $item[4] : 0;
            $tabla_item->save();
        }

        $gestion = [
            ["D1", "<p>Consejero Académico (5 puntos/año)</p>", 20],
            ["D2", "<p>Coordinador de Carrera (10 puntos /año)</p>", 30],
            ["D3", "<p>Gestión en el nivel de la educación objeto de la convocatoria (10 puntos/ año)</p>", 30],
            ["D4", "<p>Integrante de Comisiones o Redes del nivel educativo objeto de la convocatoria (5 puntos/año)</p>", 20],
            ["D5", "<p>Integrante de Comisiones Directivas de Asociaciones y/o Colegios profesionales, u ONG's (5 puntos/año)</p>", 20],
        ];

        $gest = factory(TablaPonderacion::class)->create([
            "nombre" => "Actividades de gestión",
            "letra" => "D",
            "puntaje_maximo" => 120
        ]);

        foreach ($gestion as $item){
            $tabla_item = new TablaPonderacionItem();
            $tabla_item->codigo_item = $item[0];
            $tabla_item->nombre_item = $item[1];
            $tabla_item->puntaje_maximo = $item[2];
            $tabla_item->tabla_ponderacion_id = $gest->id;
            $tabla_item->puntaje_minimo = isset($item[4]) ? $item[4] : 0;
            $tabla_item->save();
        }
    }
}
