<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\TipoUpload::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->word,
    ];
});
