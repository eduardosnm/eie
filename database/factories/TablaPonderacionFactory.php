<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\TablaPonderacion;
use Faker\Generator as Faker;

$factory->define(TablaPonderacion::class, function (Faker $faker) {
    return [
        "nombre" => $faker->name,
        "letra" => $faker->randomLetter,
        "puntaje_maximo" => $faker->randomNumber(3)
    ];
});
