<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Llamado::class, function (Faker $faker) {
    $fecha = \Carbon\Carbon::now();
    $fecha_inicio = $faker->dateTimeBetween($fecha, $fecha->addDays(7));
    return [
        "titulo" => $faker->word,
        "descripcion" => $faker->text(60),
        "estado" => true,
        "fecha_inicio" => $faker->date($fecha_inicio->format("Y-m-d")),
        "fecha_fin" => $fecha_inicio->modify('+7 days'),
        "user_id" => User::all()->random()->id,
    ];
});
