<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('Admin\\')
    ->group(function (){
        Route::get('/', 'PostulacionesController@registrar')->name('postulantes.llamados');
        Route::get('/registro/{llamado}', 'PostulacionesController@carreras')
            ->where('llamado','\d')
            ->name('postulantes.carreras');
        Route::post('/registro', 'PostulacionesController@guardar')->name('postulantes.guardar');
        Route::get('registro/{uuid}/comprobante', 'PostulacionesController@constancia')
            ->where('uuid', '[0-9a-fA-F]{8}-?[0-9a-fA-F]{4}-?4[0-9a-fA-F]{3}-?[89abAB][0-9a-fA-F]{3}-?[0-9a-fA-F]{12}')
            ->name('postulantes.constancia');
        Route::get('registro/{uuid}/imprimir', 'PostulacionesController@imprimir')
            ->where('uuid', '[0-9a-fA-F]{8}-?[0-9a-fA-F]{4}-?4[0-9a-fA-F]{3}-?[89abAB][0-9a-fA-F]{3}-?[0-9a-fA-F]{12}')
            ->name('postulantes.imprimir');
        Route::get('/registro/manuales', 'PostulacionesController@getManuales')->name('postulantes.manuales');
    });


Route::middleware('auth')
    ->namespace('Admin\\')
    ->group(function (){

        //USUARIOS
        Route::get('/usuarios', 'UsuariosController@listado')->name('usuarios.listado');
        Route::post('/usuarios','UsuariosController@crear')->name('usuarios.crear');
        Route::delete('/usuarios/{user}', 'UsuariosController@eliminar')->name('usuarios.eliminar');
        Route::get('/usuarios/{user}/editar', 'UsuariosController@editar')->name('usuarios.editar');
        Route::put('/usuarios/{user}', 'UsuariosController@actualizar')->name('usuarios.actualizar');
        Route::get('/usuarios/getUsuariosJson', 'UsuariosController@getUsuariosJson')->name('usuarios.getjson');
        Route::get('/usuarios/manuales', 'UsuariosController@getManuales')->name('usuarios.manuales');

        //CARRERAS
        Route::get('carreras', 'CarrerasController@listado')->name('carreras.listado');
        Route::get('carreras/{carrera}', 'CarrerasController@ver')
            ->where('carrera','\d')
            ->name('carreras.ver');
        Route::get('carreras/nueva', 'CarrerasController@crear')->name('carreras.crear');
        Route::post('carreras', 'CarrerasController@guardar')->name('carreras.guardar');
        Route::get('carreras/{carrera}/editar', 'CarrerasController@editar')->name('carreras.editar');
        Route::put('carreras/{carrera}', 'CarrerasController@actualizar')->name('carreras.actualizar');
        Route::delete('carreras/{carrera}', 'CarrerasController@eliminar')->name('carreras.eliminar');

        // ASIGNATURAS
        Route::get('asignaturas', 'AsignaturasController@listado')->name('asignaturas.listado');
        Route::get('asignaturas/nueva', 'AsignaturasController@crear')->name('asignaturas.crear');
        Route::post('asignaturas', 'AsignaturasController@guardar')->name('asignaturas.guardar');
        Route::get('asignaturas/{asignatura}/editar', 'AsignaturasController@editar')->name('asignaturas.editar');
        Route::put('asignaturas/{asignatura}', 'AsignaturasController@actualizar')->name('asignaturas.actualizar');
        Route::delete('asignaturas/{asignatura}', 'AsignaturasController@eliminar')->name('asignaturas.eliminar');

        // LLAMADOS
        Route::get('llamados', 'LlamadosController@listado')->name('llamados.listado');
        Route::get('llamados/nuevo', 'LlamadosController@crear')->name('llamados.crear');
        Route::get('llamados/{llamado}', 'LlamadosController@ver')->name('llamados.ver');
        Route::post('llamados', 'LlamadosController@guardar')->name('llamados.guardar');
        Route::get('llamados/{llamado}/editar', 'LlamadosController@editar')->name('llamados.editar');
        Route::get('llamados/{llamado}/editar-tribunal', 'LlamadosController@editarTribunal')->name('llamados.editarTribunal');
        Route::put('llamados/{llamado}', 'LlamadosController@actualizar')->name('llamados.actualizar');
        Route::put('llamados/{llamado}/actualizar-tribunal', 'LlamadosController@actualizarTribunal')->name('llamados.actualizarTribunal');
        Route::delete('llamados/{llamado}', 'LlamadosController@eliminar')->name('llamados.eliminar');
        Route::get('llamados/getLlamadoJson', 'LlamadosController@getLlamadoJson')->name('llamados.getjson');

        //EVALUADORES
        Route::get('evaluadores', 'EvaluadoresController@listado')->name('evaluadores.listado');
        Route::get('evaluadores/{evaluador}', 'EvaluadoresController@ver')->name('evaluadores.ver');
        Route::post('evaluadores', 'EvaluadoresController@guardar')->name('evaluadores.guardar');
        Route::put('evaluadores/{evaluador}', 'EvaluadoresController@actualizar')->name('evaluadores.actualizar');
        Route::delete('evaluadores/{evaluador}', 'EvaluadoresController@eliminar')->name('evaluadores.eliminar');

        // TRIBUNAL
        Route::get('tribunal/crear', 'TribunalesController@crear')->name('tribunales.crear');
        Route::get('tribunal/getCarreras/{id}', 'TribunalesController@getCarreras')->name('tribunales.getCarreras');
        Route::post('tribunal', 'TribunalesController@guardar')->name('tribunales.guardar');

        //POSTULANTES
        Route::get('postulaciones', 'PostulacionesController@listado')->name('postulaciones.listado');
        Route::get('postulaciones/{postulacion}/', 'PostulacionesController@ver')->name('postulaciones.ver');
        Route::get('postulaciones/curriculum/{cv}', 'PostulacionesController@getCv')->name('postulaciones.getCv');
        Route::get('postulaciones/curriculum/{cv}/descargar', 'PostulacionesController@descargarCv')->name('postulaciones.descargarCv');
        Route::get('postulaciones/{postulacion}/asignatura/{asignatura}/evaluar', 'PostulacionesController@evaluar')->name('postulaciones.evaluar');
        Route::put('postulaciones/{asignatura_postulacion}', 'PostulacionesController@guardarPuntaje')->name('postulaciones.guardarPuntaje');
        Route::post('postulaciones/editando/{asignatura_postulacion}/', 'PostulacionesController@editando')->name('postulaciones.editando');


        //INFORMES
        Route::get('informes/{llamado}/pdf', 'InformesController@listado')->name('informes.listado');
        Route::post('informes/{llamado}/pdf', 'InformesController@generarPdf')->name('informes.generarPdf');
        Route::get('informes/{llamado}/xls', 'InformesController@listadoExcel')->name('informes.listadoExcel');
        Route::post('informes/{llamado}/xls', 'InformesController@generarExcel')->name('informes.generarExcel');


        //TABLA
//        Route::get('tabla', 'TablaPonderacionController@listado')->name('tabla.listado');
//        Route::post('tabla', 'TablaPonderacionController@guardar')->name('tabla.guardar');
//        Route::get('/tabla/{tabla}/editar', 'TablaPonderacionController@editar')->name('tabla.editar');
//        Route::put('/tabla/{tabla}', 'TablaPonderacionController@actualizar')->name('tabla.actualizar');
//        Route::delete('/tabla/{tabla}', 'TablaPonderacionController@eliminar')->name('tabla.eliminar');
//        Route::get('/tabla/{tabla}', 'TablaPonderacionController@agregarSubitem')->name('tabla.agregarSubitem');
    });


Auth::routes();

