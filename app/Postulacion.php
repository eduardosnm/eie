<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Postulacion extends Model
{
    protected $table = 'postulaciones';

    protected $guarded = [];

    public function postulante()
    {
        return $this->belongsTo(Postulante::class, 'postulante_id');
    }

    public function llamado()
    {
        return $this->belongsTo(Llamado::class, 'llamado_id');
    }

    public function upload()
    {
        return $this->belongsTo(Upload::class);
    }

    public function asignaturas_postulaciones()
    {
        return $this->hasMany(AsignaturaPostulacion::class, 'postulacion_id');
    }

    public function puntajeTotal()
    {
        return $this->hasManyThrough(Puntaje::class, AsignaturaPostulacion::class, 'id', 'asignatura_postulacion_id');
    }

    public function asignaturas()
    {
        return $this->belongsToMany(Asignatura::class)
            ->using(AsignaturaPostulacion::class)
            ->withPivot('id', 'postulacion_id', 'asignatura_id');
    }

    public function scopeOrdenarPuntajeDesc($query, Llamado $llamado, Asignatura $asignatura)
    {
        return $query->join('asignatura_postulacion as ap', 'postulaciones.id', '=', 'ap.postulacion_id')
            ->join('asignatura_tabla_puntajes as atp', 'ap.id', '=', 'atp.asignatura_postulacion_id')
            ->selectRaw('postulaciones.id, postulaciones.postulante_id, postulaciones.llamado_id, SUM(atp.puntaje) as puntaje')
            ->where('postulaciones.llamado_id',$llamado->id)
            ->where('ap.asignatura_id', $asignatura->id)
            ->groupBy('postulaciones.id', 'postulaciones.postulante_id', 'postulaciones.llamado_id')
            ->orderBy(DB::raw("SUM(atp.puntaje)"), 'DESC');
//        return $query->join('asignatura_postulacion as ap', 'postulaciones.id', '=', 'ap.postulacion_id')
//            ->join('asignatura_tabla_puntajes as atp', 'ap.id', '=', 'atp.asignatura_postulacion_id')
//            ->select('postulaciones.*')
//            ->orderBy(DB::raw("SUM(atp.puntaje)"), 'DESC')
//            ->groupBy('postulaciones.id');


    }

    public function scopeFiltroEvaluadores($query)
    {
        if (auth()->user()->isAn('admin')){
            return $query;
        }

        $llamados = auth()->user()->llamados()->get()->pluck('id');
        $llamados = !$llamados->isEmpty() ? $llamados : null;

        return $query->whereHas("asignaturas", function ($q){
            $q->whereIn('carrera_id', auth()->user()->tribunales->pluck('carrera_id'));
        })->where('llamado_id', $llamados);
    }

    public function scopeFiltroBusqueda($query, Request $request)
    {
        if ($request->has('postulante') && !is_null($request->get('postulante'))){
            $query->whereHas("postulante", function ($q) use ($request){
                $q->where('nombre', 'LIKE', "%{$request->get('postulante')}%")
                    ->orWhere('apellido', 'LIKE', "%{$request->get('postulante')}%");
            });
        }
        if ($request->has('llamado') && !is_null($request->get('llamado'))){
            $query->whereHas("llamado", function ($q) use ($request){
                $q->where("id",$request->get('llamado'));
            });
        }
        return $query;
    }

    public function getNroComprobanteAttribute()
    {
        return str_pad($this->id, 5, '0', STR_PAD_LEFT);
    }

    public static function getInstanciaUuid($uuid)
    {
        return self::where("uuid", $uuid)
        ->with('asignaturas', 'llamado')
        ->firstOrFail();
    }

}
