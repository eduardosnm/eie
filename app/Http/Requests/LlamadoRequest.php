<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LlamadoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "titulo" => "required|string",
            "descripcion" => "required|string",
            "fecha_inicio" => "required|date",
            "fecha_fin" => "required|date|after:fecha_inicio",
            "asignaturas" => "present|array",
            "asignaturas.*" => "required|distinct",
            "file.reglamento" => "required|file|max:20480|mimes:pdf",
            "file.tabla" => "required|file|max:20480|mimes:pdf",
        ];

        if ($this->getMethod() == 'PUT'){
            $rules["file.reglamento"] = "nullable|file|max:20480|mimes:pdf";
            $rules["file.tabla"] = "nullable|file|max:20480|mimes:pdf";
        }

        return $rules;
    }
}
