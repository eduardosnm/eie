<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarreraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getMethod() == 'PUT'){
            return [
                'nombre' => 'required|string|unique:carreras,nombre,'.$this->carrera->id,
                'asignatura.*' => 'required|distinct',
                'estado' => 'required|boolean'
            ];
        }

        return [
            'nombre' => 'required|string|unique:carreras,nombre',
            'asignatura.*' => 'required|distinct'
        ];
    }

    public function messages()
    {
        $messages = [];
        foreach ($this->request->get('asignatura') as $key => $asignatura){
            $messages["asignatura.{$key}"] = "El campo asignatura {$key} se encuentra duplicado";
        }

        return $messages;
    }

    public function attributes()
    {
        return [
            "nombre" => "nombre de la carrera",
        ];
    }
}
