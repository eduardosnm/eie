<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostulacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre" => "required",
            "apellido" => "required",
            "telefono" => "required",
            "email" => "required",
            "cuil_cuit" => "required",
            "fecha_nacimiento" => "required|date",
            "lugar_nacimiento" => "required",
            "domicilio" => "required",
            "llamado_id" => "required|exists:llamados,id",
            "titulos_grado.*" => "required",
            "asignaturas" => "required",
            "asignaturas.*" => "required|exists:asignaturas,id",
            "file" => "required|file|max:2048|mimes:pdf",
            'g-recaptcha-response' => 'required|captcha'
        ];
    }

    public function messages()
    {
        return [
            'g-recaptcha-response.required' => 'Compruebe que no es un robot',
            'g-recaptcha-response.captcha' => 'Valide el captcha',
        ];
    }
}
