<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => "required|unique:users,email,NULL,id,deleted_at,NULL",
            'password' => 'required|max:20|confirmed',
        ];

        if ($this->getMethod() == 'PUT') {

            if ($this->request->has('password')){
                return[
                    'password' => 'required|max:20|confirmed',
                ];
            }
            unset($rules['password']);
            $rules['email'] = 'required|unique:users,email,'. $this->user->id;
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'username' => 'Nombre de usuario',
            'password' => 'contraseña',
        ];
    }
}
