<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TribunalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "llamado_id" => "required|exists:llamados,id",
            "carrera.*.id" => "required|exists:carreras,id",
            "carrera.*.evaluador.*.id" => "required|exists:users,id"
        ];
    }
}
