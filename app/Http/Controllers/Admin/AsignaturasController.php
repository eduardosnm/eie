<?php

namespace App\Http\Controllers\Admin;

use App\Asignatura;
use App\Carrera;
use App\Http\Requests\AsignaturaRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;
use Silber\Bouncer\BouncerFacade;
use function Sodium\compare;

class AsignaturasController extends Controller
{
    public function listado(Request $request)
    {
        BouncerFacade::authorize('listado-asignaturas');
        $asignaturas = Asignatura::with('carrera')
            ->buscar($request)
            ->paginate(20);

        return view('admin.asignaturas.listado', compact('asignaturas'));
    }

    public function crear()
    {
        BouncerFacade::authorize('crear-asignaturas');
        $carreras = Carrera::whereEstado(true)->get();

        return view('admin.asignaturas.crear', compact('carreras'));
    }

    public function guardar(AsignaturaRequest $request)
    {
        BouncerFacade::authorize('guardar-asignaturas');
        DB::beginTransaction();
        try{
            $carrera = Carrera::findOrFail($request->get('carrera_id'));

            foreach ($request->get('asignatura') as $materia){

                if(!is_null($carrera->estaMateriaAsociada($materia))){

                    return redirect(route('asignaturas.crear'))
                        ->withErrors("La asignatura {$materia} ya se encuentra asociada a la carrera {$carrera->nombre}")
                        ->withInput();
                }

                $asignatura = new Asignatura(["nombre"=>$materia]);
                $carrera->asignaturas()->save($asignatura);
            }
        }catch (\Exception $ex){
            DB::rollBack();
            return redirect(route('asignaturas.crear'))
                ->withErrors($ex->getMessage());
        }
        DB::commit();

        return redirect(route('asignaturas.crear'))->with('success', "La operación se registro correctamente");
    }

    public function editar(Asignatura $asignatura)
    {
        BouncerFacade::authorize('editar-asignaturas');
        $asignatura->load('carrera');
        $carreras = Carrera::whereEstado(true)->get();

        return view('admin.asignaturas.editar', compact('asignatura', 'carreras'));
    }

    public function actualizar(AsignaturaRequest $request, Asignatura $asignatura)
    {
        BouncerFacade::authorize('actualizar-asignaturas');
        DB::beginTransaction();
        try{
            $carrera = Carrera::findOrFail($request->get('carrera_id'));

            $existe = $carrera->estaMateriaAsociada($request->get('nombre'));

            if ( !is_null($existe)){
                if ( $existe->id != $asignatura->id ){
                    return redirect(route('asignaturas.editar', $asignatura))
                        ->withErrors("La asignatura {$request->get('nombre')} ya se encuentra asociada a la carrera {$carrera->nombre}")
                        ->withInput();
                }
            }

            $asignatura->update([
                "nombre" => $request->get('nombre'),
                "carrera_id" => $request->get('carrera_id'),
                "estado" => $request->get('estado')
            ]);

        }catch (\Exception $ex){
            DB::rollBack();
            return redirect(route('asignaturas.editar', $asignatura))
                ->withErrors($ex->getMessage());
        }
        DB::commit();
        return redirect(route('asignaturas.listado'))->with('success', "La operación se registro correctamente");
    }

    public function eliminar(Asignatura $asignatura)
    {
        BouncerFacade::authorize('eliminar-asignaturas');
        DB::transaction(function () use ($asignatura){
            $asignatura->delete();
        });

        return redirect(route('asignaturas.listado'))->with('success', 'La operación se realizo correctamente');
    }
}
