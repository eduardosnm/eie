<?php

namespace App\Http\Controllers\Admin;

use App\Asignatura;
use App\Carrera;
use App\Http\Requests\CarreraRequest;
use App\Upload;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\BouncerFacade;

class CarrerasController extends Controller
{
    public function listado(Request $request)
    {
        BouncerFacade::authorize('listado-carreras');
        $carreras = Carrera::buscarCarrera($request)
                ->paginate(20);

        return view('admin.carreras.listado', compact('carreras'));
    }

    public function ver(Carrera $carrera)
    {
        BouncerFacade::authorize('ver-carreras');
        $carrera->load('asignaturas');

        return view('admin.carreras.ver', compact('carrera'));
    }

    public function crear()
    {
        BouncerFacade::authorize('crear-carreras');
        $usuarios = User::all();
        return view('admin.carreras.crear', compact('usuarios'));
    }

    public function guardar(CarreraRequest $request)
    {
        BouncerFacade::authorize('guardar-carreras');
        DB::transaction(function () use ($request){
            $carrera = Carrera::create(["nombre"=>$request->get('nombre')]);

            foreach ($request->get('asignatura') as $materia){
                $asignatura = new Asignatura(["nombre"=>$materia]);
                $carrera->asignaturas()->save($asignatura);
            }
        });

        return redirect(route('carreras.crear'))->with('success', "La operación se registro correctamente");
    }

    public function editar(Carrera $carrera)
    {
        BouncerFacade::authorize('editar-carreras');
        $carrera->load('asignaturas');

        return view('admin.carreras.editar', compact('carrera'));
    }

    public function actualizar(Carrera $carrera, CarreraRequest $request)
    {
        BouncerFacade::authorize('actualizar-carreras');
        DB::transaction(function () use ($request, $carrera){

            $carrera->update([
                "nombre" => $request->get('nombre'),
                "estado" => $request->get('estado')
            ]);

            $ids = [];
            foreach ($request->get('asignatura') as $materia){
                Asignatura::find($materia['id'])->update(["nombre" => $materia['nombre']]);
                $ids[] = $materia['id'];
            }

            $carrera->asignaturas()->whereNotIn('id', $ids)->delete();

            if ($request->has('nuevo')){
                foreach ($request->get('nuevo') as  $nuevo){
                    $asignatura = new Asignatura(["nombre"=>$nuevo]);
                    $carrera->asignaturas()->save($asignatura);
                }
            }
        });

        return redirect(route('carreras.listado'))->with('success', "La operación se registro correctamente");
    }

    public function eliminar(Carrera $carrera)
    {
        BouncerFacade::authorize('eliminar-carreras');
        DB::transaction(function () use ($carrera){
            $carrera->delete();
        });

        return redirect(route('carreras.listado'))->with('success', 'La operación se realizo correctamente');
    }
}
