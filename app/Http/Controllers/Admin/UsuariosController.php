<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Upload;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Silber\Bouncer\BouncerFacade;

class UsuariosController extends Controller
{
    public function listado(Request $request)
    {
        BouncerFacade::authorize('listado-usuarios');
        $usuarios = User::whereIs('admin')
                ->buscarUsuario($request)
                ->paginate(20);

        return view('admin.usuarios.listado', compact('usuarios'));
    }

    public function crear(UserRequest $request)
    {
        BouncerFacade::authorize('crear-usuarios');
        $user = User::create([
            'nombre' => $request->get('nombre'),
            'apellido' => $request->get('apellido'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        $user->assign('admin');

        return redirect(route('usuarios.listado'))
            ->with('success', 'Operacion realizada correctamente');
    }

    public function editar(User $user)
    {
        BouncerFacade::authorize('editar-usuarios');
        return response()->json($user, 201);
    }

    public function actualizar(User $user, UserRequest $request)
    {
        BouncerFacade::authorize('actualizar-usuarios');
        if ($request->has('password')){
            $user->update(['password' => bcrypt($request->get('password'))]);
        }else{
            $user->update([
                'nombre' => $request->get('nombre'),
                'apellido' => $request->get('apellido'),
                'email' => $request->get('email'),
                'estado' => $request->has('estado') ? true : false,
            ]);
        }


        return redirect(route('usuarios.listado'))->with("success", "El usuario se modificó correctamente");
    }

    public function eliminar(User $user)
    {
        BouncerFacade::authorize('eliminar-usuarios');
        $user->delete();

        return redirect(route('usuarios.listado'))
            ->with("success", "El usuario ha sido eliminado");
    }

    public function getUsuariosJson(Request $request)
    {
        BouncerFacade::authorize('getUsuariosJson-usuarios');
        $term = $request->get('term');

        $usuario = User::select('id',DB::raw("CONCAT('apellido', ' ', 'nombre') as value"))
            ->where("apellido", "LIKE", "%{$term}%")
            ->orWhere("nombre", "LIKE", "%{$term}%")
            ->get();

        return response()->json($usuario,200);
    }

    public function getManuales()
    {
        if ( auth()->user()->isAn('admin')){
            return Storage::download("manuales/admin.pdf", "instructivo-admin.pdf");
        }
        return Storage::download("manuales/evaluadores.pdf", "instructivo-eval.pdf");
    }
}
