<?php

namespace App\Http\Controllers\Admin;

use App\Asignatura;
use App\AsignaturaPostulacion;
use App\Http\Requests\PostulacionRequest;
use App\Llamado;
use App\Http\Controllers\Controller;
use App\Notifications\EnviarConstanciaInscripcion;
use App\Postulacion;
use App\Postulante;
use App\TablaPonderacion;
use App\TablaPonderacionItem;
use App\Upload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Silber\Bouncer\BouncerFacade;

class PostulacionesController extends Controller
{

    public function listado(Request $request)
    {
        BouncerFacade::authorize('listado-postulantes');
        $result = Postulacion::with("llamado", "postulante")
            ->filtroEvaluadores()
            ->filtroBusqueda($request);

        $cantidad_inscriptos = $result->count();

        $postulaciones = $result->paginate(20);

        $llamados = auth()->user()->isAn('admin') ? Llamado::all() : auth()->user()->llamados()->get();

        return view('admin.postulaciones.listado',
            compact('postulaciones', 'llamados', 'cantidad_inscriptos'));
    }

    public function registrar()
    {
        $llamados = Llamado::whereEstado(true)->get();
        $hoy = Carbon::now();

        return view('postulantes.llamados', compact('llamados', 'hoy'));
    }

    public function guardar(PostulacionRequest $request)
    {
        $postulacion = null;

        DB::transaction(function () use ($request, &$postulacion){

            $postulante = Postulante::firstOrCreate(
                ["dni" => $request->get('dni')],
                [
                    "nombre" => $request->get('nombre'),
                    "apellido" => $request->get('apellido'),
                    "email" => $request->get('email'),
                    "cuil_cuit" => $request->get('cuil_cuit'),
                    "fecha_nacimiento" => $request->get('fecha_nacimiento'),
                    "lugar_nacimiento" => $request->get('lugar_nacimiento'),
                    "domicilio" => $request->get('domicilio'),
                    "telefono" => $request->get('telefono'),
                    "titulos_grado" => json_encode($request->get('titulos_grado'))
                ]);

            if ($postulante->esta_inscripto($request->get('llamado_id'))){
                return redirect(route('postulantes.registro'))->withErrors("Usted ya registra una inscripción para el llamado que seleccionó");
            }

            $cv = Upload::guardarCv($request, $postulante);

            $asignaturas = $request->get('asignaturas');

            $result = Postulacion::create([
                "postulante_id" => $postulante->id,
                "llamado_id" => $request->get('llamado_id'),
                "upload_id" => $cv->id,
                "uuid" => (string) Str::orderedUuid()
            ]);
            $postulacion = clone $result;
            $result->asignaturas()->attach($asignaturas);

            $postulante->update([
                "nombre" => $request->get('nombre'),
                "apellido" => $request->get('apellido'),
                "email" => $request->get('email'),
                "telefono" => $request->get('telefono'),
                "domicilio" => $request->get('domicilio'),
                "titulos_grado" => json_encode($request->get('titulos_grado'))
            ]);

            $postulante->notify(new EnviarConstanciaInscripcion($result, $postulante));
        });

        return redirect()->route('postulantes.constancia', ['uuid' => $postulacion->uuid]);
    }

    public function ver(Postulacion $postulacion)
    {
        BouncerFacade::authorize('ver-postulantes');
        $postulacion->load("postulante");

        $asignaturas = auth()->user()->isAn('admin') ? $postulacion->asignaturas
            : $postulacion->asignaturas->whereIn("carrera_id", auth()->user()->tribunales->pluck('carrera_id'));


        return view('admin.postulaciones.ver', compact('postulacion', 'asignaturas'));
    }

    public function evaluar(Postulacion $postulacion, Asignatura $asignatura)
    {
        BouncerFacade::authorize('evaluar-postulantes');
        $asignatura_postulacion = AsignaturaPostulacion::where('postulacion_id', $postulacion->id)
            ->where('asignatura_id', $asignatura->id)
            ->first();

        if (is_null($asignatura_postulacion->editando)){
            $asignatura_postulacion->editando = Carbon::now();
            $asignatura_postulacion->user_id = auth()->user()->id;
            $asignatura_postulacion->save();
        }

        if( !$asignatura_postulacion->cumplioElTiempo() && !$asignatura_postulacion->usuarioLoEstabaEditando()){
            return redirect()->route('postulaciones.ver', $postulacion)
                ->withErrors("No puede ingresar a la asignatura {$asignatura->nombre} debido a que el usuario {$asignatura_postulacion->usuario->full_name} está evaluandola. 
                Espere 5 minutos y vuelva a intentarlo");
        }

        $asignatura_postulacion->editando = Carbon::now();
        $asignatura_postulacion->user_id = auth()->user()->id;
        $asignatura_postulacion->save();



        $tabla_ponderacion = TablaPonderacion::all();

        return view('admin.postulaciones.tabla', compact('postulacion', 'asignatura', 'tabla_ponderacion', 'asignatura_postulacion'));
    }

    public function guardarPuntaje(AsignaturaPostulacion $asignatura_postulacion, Request $request)
    {
        BouncerFacade::authorize('guardarPuntaje-postulantes');
        DB::transaction(function () use ($request, $asignatura_postulacion){

            DB::table('asignatura_tabla_puntajes')
                ->where('asignatura_postulacion_id', $asignatura_postulacion->id)
                ->delete();

            foreach ($request->get('asig_post') as $asig_post => $subitems){
                foreach ($subitems['subitem'] as $nroSubItem => $puntaje){
                    $subitem = TablaPonderacionItem::find($nroSubItem);

                    DB::table('asignatura_tabla_puntajes')->insert([
                        "puntaje" => $puntaje,
                        "asignatura_postulacion_id" => $asig_post,
                        "tabla_item_id" => $subitem->id,
                        "tabla_ponderacion_id" => $subitem->tabla_poderacion->id,
                        "created_at" => Carbon::now(),
                        "user_id" => auth()->user()->id
                    ]);
                }
            }

            $asignatura_postulacion->editando = null;
            $asignatura_postulacion->user_id = null;
            $asignatura_postulacion->save();
        });

        return redirect()->route('postulaciones.ver', $asignatura_postulacion->postulacion_id)
            ->with("success", "Los cambios se produjeron exitosamente");
    }

    public function editando(AsignaturaPostulacion $asignaturaPostulacion, Request $request)
    {
        $asignaturaPostulacion->editando = $request->get('editando');
        $asignaturaPostulacion->user_id = auth()->user()->id;
        $asignaturaPostulacion->save();

        return $asignaturaPostulacion->toJson();
    }

    public function carreras(Llamado $llamado)
    {
        if (!$llamado->estado){
            return redirect()->route('postulantes.llamados')->withErrors("El llamado no está activo");
        }
        if ($llamado->fecha_inicio->gt(Carbon::now())){
            return redirect()->route('postulantes.llamados')->withErrors("Usted se podrá inscribir a partir del dia {$llamado->fecha_inicio->format('d/m/Y')}");
        }
        $carreras = null;
        foreach ($llamado->asignaturas as $asignatura){
            $carreras[$asignatura->carrera->nombre][] = $asignatura;
        }

        return view('postulantes.registro', compact('carreras', 'llamado'));
    }

    public function getCv(Upload $cv)
    {
        BouncerFacade::authorize('getCv-postulantes');
        $filePath = $cv->url;

        if(!Storage::exists($filePath)){
            abort(404);
        }

        $pdfContent = Storage::get($filePath);

        return Response::make($pdfContent, 200, [
            'Content-Type'        => $cv->mime,
            'Content-Disposition' => 'inline; filename="'.$cv->nombre.'"'
        ]);
    }

    public function descargarCv(Upload $cv)
    {
        BouncerFacade::authorize('descargarCv-postulantes');
        if(!Storage::exists($cv->url)){
            abort(404);
        }

        return Storage::download($cv->url, "{$cv->nombre}");
    }

    public function constancia($uuid)
    {
        $postulacion = Postulacion::getInstanciaUuid($uuid);

        return view('postulantes.finalizacion', compact('postulacion'));
    }

    public function imprimir($uuid)
    {
        $postulacion = Postulacion::getInstanciaUuid($uuid);

        $html = view('postulantes.comprobante', compact('postulacion'))->render();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHtml($html)->setPaper('a4', 'portrait');

        return $pdf->stream('comprobante');
    }

    public function getManuales()
    {
        return Storage::download("manuales/postulantes.pdf", "instructivo-postulantes.pdf");
    }
}
