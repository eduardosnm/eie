<?php

namespace App\Http\Controllers\Admin;

use App\Asignatura;
use App\Carrera;
use App\Exports\PostulacionesExport;
use App\Fpdf\Fpdf;
use App\Llamado;
use App\Postulante;
use App\TablaPonderacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Excel;
use Silber\Bouncer\BouncerFacade;

class InformesController extends Controller
{
    public function listado(Llamado $llamado)
    {
        BouncerFacade::authorize('generarPdf-informes');
        $llamado->load('asignaturas');
        $result = [];
        foreach ($llamado->asignaturas as $asignatura){
            $result[$asignatura->carrera->nombre][] = $asignatura;
        }

        return view('admin.informes.asignaturas', compact('result', 'llamado'));
    }

    public function generarPdf(Llamado $llamado, Request $request)
    {
        BouncerFacade::authorize('generarPdf-informes');
        $result = $llamado->asignaturas->whereIn('id',$request->get('asignaturas'))
            ->groupBy(function ($item){
                return $item->nombre."_".$item->id;
            })->mapSpread(function ($asignatura){
                return $asignatura->postulaciones->map(function ($postulacion){
                     return $postulacion->postulante;
                })->sortBy(function($postulante) {
                    return $postulante->apellido;
                });
            });

        $pdf = new Fpdf();

        foreach ($result as $asignatura => $postulantes){
            if ($postulantes->isEmpty()){
                continue;
            }

            $asignatura = explode("_", $asignatura);
            $asig = Asignatura::find($asignatura[1]);
            $pdf->AddPage('Portrait');
            $pdf->setAsignatura($asignatura[0]);
            $pdf->SetFont('Courier', 'B', 14);
            $pdf->MultiCell( 180, 10, utf8_decode($llamado->titulo), 0, 'C');
            $pdf->SetFont('Courier', 'B', 12);
            $pdf->MultiCell( 180, 10, utf8_decode($asig->carrera->nombre), 0, 'C');
            $pdf->MultiCell( 180, 10, utf8_decode($asignatura[0]), 0, 'C');
            $pdf->SetFont('Courier', '', 10);
            $pdf->Ln();
            $pdf->Cell(40,10,'Fecha: '.Carbon::now()->format("d/m/Y"),0,0,'L');
            $pdf->Ln();
            $headers = ['DNI', 'Postulante'];
            $w = array(40, 150);
            $pdf->SetFillColor(220,220,220);
            $pdf->SetTextColor(0);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('','B');
            for($i=0;$i<count($headers);$i++){
                $pdf->Cell($w[$i],7,$headers[$i],1,0,'C',true);
            }
            $pdf->Ln();
            $pdf->SetFillColor(220,220,220);
            $pdf->SetTextColor(0);
            $pdf->SetFont('', '', 10);

            $fill = false;
            foreach ($postulantes as $postulante){
                $pdf->Cell($w[0],5,$postulante->dni,'LR',0,'C',$fill);
                $pdf->Cell($w[1],5,utf8_decode( $postulante->full_name ) ,'LR',0,'L',$fill);
                $pdf->Ln();
                $fill = !$fill;
            }
            $pdf->Cell(array_sum($w),0,'','T');
        }


        $pdf->Output();
        exit();
    }

    public function listadoExcel(Llamado $llamado)
    {
        BouncerFacade::authorize('generarExcel-informes');
        $llamado->load('asignaturas');
        $result = [];
        foreach ($llamado->asignaturas as $asignatura){
            $result[$asignatura->carrera->nombre][] = $asignatura;
        }

        return view('admin.informes.asignaturasExcel', compact('result', 'llamado'));
    }

    public function generarExcel(Llamado $llamado, Request $request)
    {
        BouncerFacade::authorize('generarExcel-informes');
        $asignatura = Asignatura::where('id',$request->get('asignaturas'))->first();

        $result = $llamado->postulaciones()
                ->whereHas('asignaturas_postulaciones', function ($ap) use ($asignatura){
                return $ap->where('asignatura_id', $asignatura->id);
            })
            ->ordenarPuntajeDesc($llamado, $asignatura)
            ->get()
            ->map(function ($postulacion) {
                return $postulacion->postulante;
            });

        return (new PostulacionesExport($result, $asignatura, $llamado))->download(Str::slug($asignatura->nombre).".xls", Excel::XLSX);
    }
}
