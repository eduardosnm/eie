<?php

namespace App\Http\Controllers\Admin;

use App\Carrera;
use App\Http\Requests\LlamadoRequest;
use App\Llamado;
use App\Tribunal;
use App\Upload;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Silber\Bouncer\BouncerFacade;

class LlamadosController extends Controller
{
    public function listado(Request $request)
    {
        BouncerFacade::authorize('listado');

        $llamados = Llamado::buscar($request)
            ->orderBy('fecha_fin', 'desc')
            ->paginate(20);

        return view('admin.llamados.listado', compact('llamados'));
    }

    public function crear()
    {
        BouncerFacade::authorize('crear');

        $carreras = Carrera::with('asignaturas')
            ->orderBy('nombre')
            ->whereEstado(true)
            ->get();

        return view('admin.llamados.crear', compact('carreras'));
    }

    public function guardar(LlamadoRequest $request)
    {
        BouncerFacade::authorize('guardar');
        DB::beginTransaction();
        try{

            $llamado = Llamado::create([
                "titulo" => $request->get('titulo'),
                "descripcion" => $request->get('descripcion'),
                "fecha_inicio" => $request->get('fecha_inicio'),
                "fecha_fin" => $request->get('fecha_fin'),
                "user_id" => auth()->user()->id
            ]);

            if ($request->has('file')){
                Upload::guardarFilesLlamado($request, $llamado);
            }

            $llamado->asignaturas()->sync($request->get('asignaturas'));

        }catch (\Exception $ex){
            DB::rollBack();
            return redirect(route('llamados.listado'))->withErrors($ex->getMessage());
        }
        DB::commit();
        return redirect(route('llamados.listado'))->with("success", "La operación se realizo correctamente");
    }

    public function editar(Llamado $llamado)
    {
        BouncerFacade::authorize('editar');
        $llamado->load('asignaturas');
        $carreras = Carrera::with('asignaturas')
            ->orderBy('nombre')
            ->whereEstado(true)
            ->get();

        return view('admin.llamados.editar', compact('llamado', 'carreras'));
    }

    public function actualizar(Llamado $llamado, LlamadoRequest $request)
    {
        BouncerFacade::authorize('actualizar');
        $llamado->update([
            "titulo" => $request->get('titulo'),
            "descripcion" => $request->get('descripcion'),
            "fecha_inicio" => $request->get('fecha_inicio'),
            "fecha_fin" => $request->get('fecha_fin'),
            "user_id" => auth()->user()->id
        ]);

        if ($request->has('file')){
            foreach ($request->file('file') as $tipo => $pdf){
                if ($tipo == 'tabla'){
                    Storage::delete($llamado->getStorageFilePath().$llamado->tabla->nombre);
                    $llamado->tabla()->delete();
                }
                if ($tipo == 'reglamento'){
                    Storage::delete($llamado->getStorageFilePath().$llamado->reglamento->nombre);
                    $llamado->reglamento()->delete();
                }
            }

            Upload::guardarFilesLlamado($request, $llamado);
        }

        $llamado->asignaturas()
            ->sync($request->get('asignaturas'));

        return redirect(route('llamados.listado'))->with("success", "El llamado se modificó correctamente");
    }

    public function actualizarTribunal(Llamado $llamado, Request $request)
    {
        BouncerFacade::authorize('actualizar-tribunal');

        DB::transaction(function () use($llamado, $request){
            Tribunal::where('llamado_id', $llamado->id)->delete();
            $carreras = $request->get('carrera');
            foreach ($carreras as $id => $evaluadores){
                foreach ($evaluadores["evaluador"] as $evaluador){
                    $tribunal = new Tribunal;
                    $tribunal->llamado_id = $llamado->id;
                    $tribunal->carrera_id = $id;
                    $tribunal->user_id = $evaluador;

                    $tribunal->save();
                }
            }
        });

        return redirect()->route('llamados.editarTribunal', $llamado)
            ->with("success", "El tribunal fue modificado exitosamente");

    }

    public function editarTribunal(Llamado $llamado)
    {
        $carreras = $llamado->tribunal->groupBy(function ($item){
            return $item->carrera->nombre."_".$item->carrera->id;
        });

        $evaluadores = User::whereIs('evaluador')
            ->select('id', DB::raw("CONCAT(apellido, ', ', nombre) AS value"))
            ->get()->toJson();

        return view('admin.llamados.editar_tribunal', compact('carreras', 'llamado', 'evaluadores'));
    }

    public function ver(Llamado $llamado)
    {
        BouncerFacade::authorize('ver');
        $result = [];
        foreach ($llamado->asignaturas as $asignatura){
            $result[$asignatura->carrera->nombre][] = $asignatura;
        }

        $carreras = $llamado->tribunal->groupBy(function ($item){
            return $item->carrera->nombre;
        });

        return view('admin.llamados.ver', compact('llamado', 'result', 'carreras'));
    }

    public function eliminar(Llamado $llamado)
    {
        BouncerFacade::authorize('eliminar');
        $llamado->delete();

        return redirect(route('llamados.listado'))
            ->with("success", "El llamado ha sido eliminado");
    }

    public function getUsuariosJson(Request $request)
    {
        BouncerFacade::authorize('getUsuariosJson');
        $term = $request->get('term');

        $usuario = User::select('id',DB::raw("CONCAT('apellido', ' ', 'nombre') as value"))
            ->where("apellido", "LIKE", "%{$term}%")
            ->orWhere("nombre", "LIKE", "%{$term}%")
            ->get();

        return response()->json($usuario,200);
    }
}
