<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Notifications\CambioEmail;
use App\Notifications\EnviarCredenciales;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Silber\Bouncer\BouncerFacade;

class EvaluadoresController extends Controller
{
    public function listado(Request $request)
    {
        BouncerFacade::authorize('listado-evaluadores');
        $evaluadores = User::whereIs('evaluador')
            ->buscarUsuario($request)
            ->paginate(20);

        return view('admin.evaluadores.listado', compact('evaluadores'));
    }

    public function ver(User $evaluador)
    {
        $llamados = $evaluador->tribunales->groupBy(function ($item){
            if ($item->llamado->estado){
                return $item->llamado->titulo;
            }
        });

        return view('admin.evaluadores.ver', compact('evaluador', 'llamados'));
    }

    public function guardar(Request $request)
    {
        BouncerFacade::authorize('guardar-evaluadores');
        $request->validate([
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => "required|unique:users,email,NULL,id,deleted_at,NULL",
        ]);

        DB::transaction(function () use ($request){
            $random_password = Str::random(8);
            $user = User::create([
                "nombre" => $request->get('nombre'),
                "apellido" => $request->get('apellido'),
                "email" => $request->get('email'),
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt($random_password),
                "password_confirmation" => bcrypt($random_password),
            ]);

            $user->assign('evaluador');

            $user->notify(new EnviarCredenciales($user, $random_password));
        });

        return redirect(route('evaluadores.listado'))
            ->with("success", "La operacion se realizo exitosamente.\n Un correo electrónico con las credenciales de acceso fue enviado al evaluador");
    }

    public function actualizar(User $evaluador, Request $request)
    {
        BouncerFacade::authorize('actualizar-evaluadores');
        $es_diferente = trim($evaluador->email)  !== trim($request->get('email'));
        $rules = [
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => $es_diferente ? 'email|unique:users,email' : 'email|unique:users,email,'.$evaluador->id,
        ];
        $request->validate($rules);

        $mensaje = "El evaluador se modificó correctamente.";
        DB::transaction(function () use ($request, $evaluador, &$mensaje, $es_diferente){
            $evaluador->nombre = $request->get('nombre');
            $evaluador->apellido = $request->get('apellido');

            if ($es_diferente){
                $evaluador->email = $request->get('email');
                $mensaje .= " Un correo de confirmación fue enviado a la dirección ".$request->get('email');
            }
            $evaluador->save();
        });

        if ($es_diferente){
            $evaluador->notify(new CambioEmail($evaluador));
        }

        return redirect(route('evaluadores.listado'))->with("success", $mensaje);
    }

    public function eliminar(User $evaluador)
    {
        BouncerFacade::authorize('eliminar-evaluadores');
        $evaluador->delete();

        return redirect(route('evaluadores.listado'))
            ->with("success", "El evaluador ha sido eliminado");
    }
}
