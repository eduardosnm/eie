<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TribunalRequest;
use App\Llamado;
use App\Tribunal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\BouncerFacade;

class TribunalesController extends Controller
{
    public function crear()
    {
        BouncerFacade::authorize('crear-tribunal');
        $evaluadores = User::whereIs('evaluador')
            ->select('id', DB::raw("CONCAT(apellido, ', ', nombre) AS value"))
            ->get()->toJson();

        $tribunales = Tribunal::pluck('llamado_id')->unique('llamado_id')->toArray();
        $llamados = Llamado::habilitados()
            ->whereNotIn('id',$tribunales)
            ->get();

        return view('admin.tribunal.crear_tribunal', compact('evaluadores', 'llamados'));
    }

    public function getCarreras($id)
    {
        BouncerFacade::authorize('getCarreras-tribunal');
        $llamado = Llamado::findOrFail($id);
        $carreras = $llamado->getCarreras();
        $evaluadores = User::whereIs('evaluador')
            ->select('id', DB::raw("CONCAT(apellido, ', ', nombre) AS value"))
            ->get()->toJson();

        if ($carreras->isEmpty()){
            return response()->json($carreras,404);
        }

        return view('admin.tribunal._carreras', compact('carreras','evaluadores'))->render();
    }

    public function guardar(TribunalRequest $request)
    {
        BouncerFacade::authorize('guardar-tribunal');
        DB::transaction(function () use ($request){

            foreach ($request->get('carrera') as $carrera){
                foreach ($carrera['evaluador'] as $evaluador){
                    $tribunal = new Tribunal;
                    $tribunal->llamado_id = $request->get('llamado_id');
                    $tribunal->carrera_id = $carrera['id'];
                    $tribunal->user_id = $evaluador['id'];

                    $tribunal->save();
                }
            }
        });

        return redirect(route('llamados.ver', $request->get('llamado_id')))
            ->with("success", "El tribunal se creo correctamente");

    }
}
