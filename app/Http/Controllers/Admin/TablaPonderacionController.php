<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TablaPonderacionRequest;
use App\TablaPonderacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\BouncerFacade;

class TablaPonderacionController extends Controller
{
    public function listado()
    {
        BouncerFacade::authorize('listado-tabla_ponderacion');
        $tabla = TablaPonderacion::paginate(20);

        return view('admin.tabla.listado', compact('tabla'));
    }

    public function guardar(TablaPonderacionRequest $request)
    {
        BouncerFacade::authorize('guardar-tabla_ponderacion');
        DB::transaction(function () use ($request){
            TablaPonderacion::create($request->all());
        });

        return redirect()->route('tabla.listado')->with("success", "El registro fue creado exitosamente");
    }

    public function editar(TablaPonderacion $tabla)
    {
        BouncerFacade::authorize('editar-tabla_ponderacion');
        return response()->json($tabla, 201);
    }

    public function actualizar(TablaPonderacion $tabla, TablaPonderacionRequest $request)
    {
        BouncerFacade::authorize('editar-tabla_ponderacion');
        DB::transaction(function () use ($request, $tabla){
            $tabla->update($request->all());
        });

        return redirect()->route('tabla.listado')->with("success", "El registro fue modificado exitosamente");
    }

    public function eliminar(TablaPonderacion $tabla)
    {
        BouncerFacade::authorize('eliminar-tabla_ponderacion');
        DB::transaction(function () use ($tabla){
            $tabla->delete();
        });

        return redirect()->route('tabla.listado')->with("success", "El registro fue eliminado exitosamente");
    }

    public function agregarSubitem(TablaPonderacion $tabla)
    {
        BouncerFacade::authorize('agregarSubitem-tabla_ponderacion');

        return view('admin.tabla.crear_subitem', compact('tabla'));
    }
}
