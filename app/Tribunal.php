<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tribunal extends Model
{
    protected $table = 'tribunales';

    protected $guarded = [];

    public function llamado()
    {
        return $this->belongsTo(Llamado::class);
    }

    public function carrera()
    {
        return $this->belongsTo(Carrera::class);
    }

    public function evaluador()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
