<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Postulante extends Model
{
    use Notifiable;

    protected $guarded = [];

    public function uploads()
    {
        return $this->morphToMany(Upload::class, 'uploadable');
    }

    public function llamados()
    {
        return $this->belongsToMany(Llamado::class, 'postulaciones');
    }

    public function postulaciones()
    {
        return $this->hasMany(Postulacion::class, 'postulante_id');
    }

    public function asignatura_postulacion()
    {
        return $this->hasManyThrough(AsignaturaPostulacion::class, Postulacion::class);
    }

    public function getFullNameAttribute()
    {
        return ucwords(strtolower($this->apellido . " " . $this->nombre));
    }
    public function getFileNameSlugAttribute()
    {
        return Str::slug($this->full_name);
    }

    public function getTitulosAttribute()
    {
        return json_decode($this->titulos_grado);
    }

    public function getFilePath()
    {
        return 'cvs/';
    }

    public function getStorageFilePath()
    {
        return 'cvs/';
    }

    public function esta_inscripto($llamado_id)
    {
        return $this->llamados()->where("llamados.id",$llamado_id)->first();
    }

    public function getPuntaje($llamado_id, $asignatura_id)
    {
//        return DB::table('postulaciones as pst')
//            ->select('pst.id as postulacion_id', DB::raw("CONCAT(pos.apellido, ' ', pos.nombre) as nombre"),
//                'tp.id as ponderacion_id', DB::raw("SUM(atp.puntaje) as puntaje"), 'pos.id as postulante_id')
//            ->join('asignatura_postulacion as ap', 'pst.id', '=', 'ap.postulacion_id')
//            ->join('asignatura_tabla_puntajes as atp', 'ap.id', '=', 'atp.asignatura_postulacion_id')
//            ->join('tabla_ponderacion as tp', 'atp.tabla_ponderacion_id', '=', 'tp.id')
//            ->join('postulantes as pos', 'pst.postulante_id', '=', 'pos.id')
//            ->where('pst.llamado_id', $llamado_id)
//            ->where('ap.asignatura_id', $asignatura_id)
//            ->groupBy('pst.id','tp.id')
//            ->get();

        return $this->postulaciones()->where('llamado_id', $llamado_id)->first()
            ->asignaturas_postulaciones->where('asignatura_id', $asignatura_id)->first();
    }
}
