<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TablaPonderacion extends Model
{
    protected $table = 'tabla_ponderacion';

    protected $guarded = [];

    public function items()
    {
        return $this->hasMany(TablaPonderacionItem::class, 'tabla_ponderacion_id');
    }
}
