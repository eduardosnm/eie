<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Llamado extends Model
{
    protected $table = 'llamados';

    protected $dates = ['fecha_inicio', 'fecha_fin'];

    protected $guarded = [];

    /**
     * RELACIONES
     */
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function asignaturas()
    {
        return $this->belongsToMany(Asignatura::class, 'asignatura_llamado');
    }

    public function carreras()
    {
        return $this->hasManyThrough(Carrera::class, Asignatura::class);
    }

    public function tribunal()
    {
        return $this->hasMany(Tribunal::class);
    }

    public function postulantes()
    {
        return $this->belongsToMany(Postulante::class, 'postulaciones');
    }

    public function postulaciones()
    {
        return $this->hasMany(Postulacion::class);
    }

    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    public function reglamento()
    {
        return $this->hasOne(Upload::class, 'uploadable_id')
            ->where('uploadable_type', self::class)
            ->where('tipo_upload_id', 3);
    }

    public function tabla()
    {
        return $this->hasOne(Upload::class, 'uploadable_id')
            ->where('uploadable_type', self::class)
            ->where('tipo_upload_id', 2);
    }

    public function scopeHabilitados($query)
    {
        return $query->whereEstado(true);
    }

    public function scopeEliminarAsignados($query)
    {
        $query->whereHas('tribunal', function($q){
            $q->whereNotIn('llamado_id');
        });
    }

    public function scopeBuscar($query, Request $request)
    {
        if ($request->has('buscar') && !is_null($request->get('buscar'))){
            $query->where('titulo', 'LIKE', "%{$request->get('buscar')}%");
        }
        return $query;
    }

    /**
     * ACCESSORS Y MUTATORS
     */

    public function getEstadoLabelAttribute()
    {
        if ($this->estado){
            return "<span class='badge badge-success'>Activo</span>";
        }
        return "<span class='badge badge-danger'>Finalizado</span>";
    }

    public function getFilePath()
    {
        return 'storage/';
    }

    public function getStorageFilePath()
    {
        return 'public/';
    }

    public function getCarreras()
    {
//        return $this->asignaturas
//            ->pluck('carrera')
//            ->flatten(1)
//            ->unique('id')
//            ->sortBy('id');
        return DB::table('carreras')
            ->select('carreras.id', 'carreras.nombre')
            ->join('asignaturas', 'carreras.id', '=','asignaturas.carrera_id')
            ->join('asignatura_llamado', 'asignaturas.id', '=', 'asignatura_llamado.asignatura_id')
            ->join('llamados', 'asignatura_llamado.llamado_id', '=', 'llamados.id')
            ->where('llamados.id', $this->id)
            ->where('carreras.estado', true)
            ->distinct()
            ->get();
    }
}
