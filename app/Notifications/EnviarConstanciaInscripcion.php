<?php

namespace App\Notifications;

use App\Postulacion;
use App\Postulante;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\HtmlString;

class EnviarConstanciaInscripcion extends Notification
{
    use Queueable;

    private $postulacion;
    private $postulante;

    /**
     * Create a new notification instance.
     *
     * @param Postulacion $postulacion
     * @param Postulante $postulante
     */
    public function __construct(Postulacion $postulacion, Postulante $postulante)
    {
        $this->postulacion = $postulacion;
        $this->postulante = $postulante;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $mailTestigo = [];
        if (App::environment('production')) {
            $mailTestigo = ['admincms@eie.unse.edu.ar'];
        }
        $mensaje = new MailMessage();
        $mensaje->cc($mailTestigo);
        $mensaje->subject('EIE: Constancia de inscripción - SISINSCDOC:['.$this->postulacion->llamado->id.'_'.$this->postulacion->nro_comprobante.'_'.$this->postulacion->created_at->format("d/m/Y H:i:s").']');
        $mensaje->greeting("{$this->postulante->full_name}, su inscripción a la \"CONVOCATORIA DE ASPIRANTES PARA CUBRIR CONTRATOS DE SERVICOS DOCENTES EVENTUALES EN LA EIE\" se realizo exitosamente.");
        $mensaje->line('Fecha: '.$this->postulacion->created_at->format("d/m/Y H:i:s"));
        $mensaje->line('Número: '.$this->postulacion->nro_comprobante);
        $mensaje->line('Llamado: '.$this->postulacion->llamado->titulo);
        $mensaje->line('Asignaturas:');
        $html = '<ul>';
        foreach($this->postulacion->asignaturas as $asignatura){
            $html .= '<li>'.$asignatura->carrera->nombre. ' - ' .$asignatura->nombre.'</li>';
        }
        $html .= '</ul>';
        $mensaje->line(new HtmlString($html));
        return $mensaje;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
