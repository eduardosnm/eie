<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    use Queueable;

    private $token;
    private $mail;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $mail)
    {
        $this->token = $token;
        $this->mail = $mail;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = route('password.reset', $this->token."?email=".$this->mail);
        return (new MailMessage)
                    ->subject('Restablecimiento de contraseña')
                    ->greeting("¡Hola!")
                    ->line('Está recibiendo este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.')
                    ->action('Restablecer contraseña', $link)
                    ->line('Este enlace de restablecimiento de contraseña caducará en 60 minutos.')
                    ->line('Si no ha solicitado un restablecimiento de contraseña, no se requiere ninguna acción adicional.')
                    ->line('Saludos')
                    ->salutation(' Escuela para la Innovación Educativa.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
