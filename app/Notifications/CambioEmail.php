<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class CambioEmail extends Notification
{
    use Queueable;

    public $evaluador;

    /**
     * Create a new notification instance.
     *
     * @param User $evaluador
     */
    public function __construct(User $evaluador)
    {
        $this->evaluador = $evaluador;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $mensaje = new MailMessage();
        $mensaje->subject('EIE: Cambio de correo electrónico');
        $mensaje->greeting("{$this->evaluador->full_name}, recibimos la solicitud de cambio de correo electrónico.");
        $mensaje->line("Para acceder al sistema deberá utilizar la dirección actual de correo electrónico y la contraseña 
                        que le fue asignada anteriormente");

        $html = "<p>Para acceder al sistema desde afuera de la EIE: <a href='https://moodle.eie.unse.edu.ar:54431/'>https://moodle.eie.unse.edu.ar:54431/</a></p>";
        $html .= "<p>Para acceder al sistema desde adentro de la EIE: <a href='http://sid.eie.unse.edu.ar/'>http://sid.eie.unse.edu.ar/</a></p>";

        $mensaje->line(new HtmlString($html));

        return $mensaje;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
