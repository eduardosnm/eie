<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class EnviarCredenciales extends Notification
{
    use Queueable;

    private $user;
    private $password;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mensaje = new MailMessage;

        $mensaje->subject('EIE: Credenciales de acceso');
        $mensaje->greeting("Estimado {$this->user->full_name}, ya puede acceder al sistema de \"CONVOCATORIA DE ASPIRANTES PARA CUBRIR CONTRATOS DE SERVICOS DOCENTES EVENTUALES EN LA EIE.\"");
        $mensaje->line("Su contraseña es: {$this->password}");
        $mensaje->line("Para acceder al sistema deberá utilizar su correo electrónico y la 
contraseña que se le asignó en este correo desde los siguientes enlaces:");

        $html = "<p>Desde fuera de la EIE: <a href='https://moodle.eie.unse.edu.ar:54431/'>https://moodle.eie.unse.edu.ar:54431/</a> o <a href='http://moodle.eie.unse.edu.ar:8031/'>http://moodle.eie.unse.edu.ar:8031/</a></p>";
        $html .= "<p>Desde dentro de la EIE: <a href='http://sid.eie.unse.edu.ar/'>http://sid.eie.unse.edu.ar/</a></p>";

        $mensaje->line(new HtmlString($html));
        $mensaje->line('Saludos.');
        $mensaje->salutation(' Escuela para la Innovación Educativa.');

        return $mensaje;
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
