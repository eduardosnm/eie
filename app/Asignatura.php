<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Asignatura extends Model
{
    protected $table = 'asignaturas';

    protected $guarded = [];

    public function carrera()
    {
        return $this->belongsTo(Carrera::class);
    }

    public function llamados()
    {
        return $this->belongsToMany(Llamado::class, 'asignatura_llamado');
    }

    public function postulaciones()
    {
        return $this->belongsToMany(Postulacion::class, 'asignatura_postulacion');
    }

    public function asignaturas_postulaciones()
    {
        return $this->hasMany(AsignaturaPostulacion::class, 'asignatura_id');
    }

    public function getEstadoLabelAttribute()
    {
        if ($this->estado){
            return "<span class='badge badge-success'>Activo</span>";
        }
        return "<span class='badge badge-danger'>Deshabilitado</span>";
    }

    public function scopeBuscar($query, Request $request)
    {
        if ($request->has('buscar') && !is_null($request->get('buscar'))){
            $query->where('nombre', 'LIKE', "%{$request->get('buscar')}%");
        }
        return $query;
    }
}
