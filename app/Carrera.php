<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Carrera extends Model
{
    protected $table = 'carreras';
    protected $guarded = [];
    // RELACIONES
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function asignaturas()
    {
        return $this->hasMany(Asignatura::class);
    }

    public function evaluadores()
    {
        return $this->belongsToMany(User::class, 'tribunales', 'carrera_id', 'user_id');
    }
    // FIN RELACIONES

    // GETTERS, SETTERS
    public function getEstadoLabelAttribute()
    {
        if ($this->estado){
            return "<span class='badge badge-success'>Activo</span>";
        }
        return "<span class='badge badge-danger'>Deshabilitado</span>";
    }

    // FIN GETTERS, SETTERS

    // SCOPES
    public function scopeBuscarCarrera($query, Request $request)
    {
        if ($request->has('buscar') && !is_null($request->get('buscar'))){
            $query->where('nombre', 'LIKE', "%{$request->get('buscar')}%");
        }
        return $query;
    }
    // FIN SCOPES

    public function estaMateriaAsociada($materia)
    {
        return $this->asignaturas()->where("nombre", $materia)->first();
    }

    public function tribunal()
    {
        return $this->belongsToMany(User::class, 'tribunales', 'carrera_id', 'user_id')->withPivot('llamado_id');
    }
}
