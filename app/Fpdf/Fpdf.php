<?php


namespace App\Fpdf;


class Fpdf extends \Codedge\Fpdf\Fpdf\Fpdf
{
    private $asignatura;

    public function setAsignatura($asignatura)
    {
        $this->asignatura = $asignatura;
    }

    public function Footer()
    {
        $this->AliasNbPages();
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode($this->asignatura) . ' - '.$this->PageNo().'/{nb}',0,0,'C');
    }

    public function SetDash($black=null, $white=null)
    {
        if($black!==null)
            $s=sprintf('[%.3F %.3F] 0 d',$black*$this->k,$white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }
}
