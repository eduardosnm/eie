<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TablaPonderacionItem extends Model
{
    protected $table = 'tabla_ponderacion_items';

    public function asignatura_postulacion()
    {
        return $this->belongsToMany(AsignaturaPostulacion::class, 'asignatura_tabla_puntajes', 'tabla_item_id' ,
            'asignatura_postulacion_id')
            ->withPivot('puntaje');
    }

    public function padre()
    {
        return $this->belongsTo(TablaPonderacionItem::class, 'parent_id');
    }

    public function hijos()
    {
        return $this->hasMany(TablaPonderacionItem::class, 'parent_id');
    }

    public function tabla_poderacion()
    {
        return $this->belongsTo(TablaPonderacion::class, 'tabla_ponderacion_id');
    }

    public function buscarPuntaje(Asignatura $asignatura)
    {
//        return $this->whereHas('asignatura_postulacion', function ($query) use ($asignatura){
//            $query->where('')
//        });
    }
}
