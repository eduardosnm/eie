<?php

namespace App;

use App\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable implements CanResetPassword
{
    use Notifiable, HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'username','apellido','email', 'password', 'estado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tribunales()
    {
        return $this->hasMany(Tribunal::class);
    }

    public function llamados()
    {
        return $this->belongsToMany(Llamado::class,'tribunales');
    }
    //SCOPES
    public function scopeBuscarUsuario($query, Request $request)
    {
        if ($request->has('buscar') && !is_null($request->get('buscar'))){
            $query->where('nombre', 'LIKE', "%{$request->get('buscar')}%")
                ->orWhere('apellido', 'LIKE', "%{$request->get('buscar')}%");
        }
        return $query;
    }

    // FIN SCOPES

    // ACCESOR Y MUTATORS
    public function getFullNameAttribute()
    {
        return "{$this->apellido} {$this->nombre}";
    }

    // FIN ACCESOR Y MUTATORS

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token, $this->email));
    }
}
