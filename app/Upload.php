<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Upload extends Model
{
    protected $guarded = [];

    public function uploadable()
    {
        return $this->morphTo();
    }

    public static function guardarCv(Request $request, Model $model)
    {
        $file = $request->file('file');

        //get file extension
        $extension = $file->getClientOriginalExtension();

        //filename to store
        $filenametostore = $model->file_name_slug . '_' . uniqid() . '.' . date("YmdHi") . '.' . $extension;

        Storage::put($model->getStorageFilePath() . $filenametostore, fopen($file, 'r+'));

        return Upload::create([
            'url' => $model->getFilePath() . $filenametostore,
            'nombre' => $filenametostore,
            'mime' => $file->getClientMimeType(),
            'extension' => $extension,
            'uploadable_id' => $model->id,
            'uploadable_type' => get_class($model),
            'tipo_upload_id' => 1
        ]);
    }

    public static function guardarFilesLlamado(Request $request, Model $model)
    {

        foreach ($request->file('file') as $tipo => $pdf){

            $file = $pdf;

            $tipo_upload = TipoUpload::where('descripcion', $tipo)->first();

            //get file extension
            $extension = $file->getClientOriginalExtension();
            $file_name = str_replace(".pdf", "", $file->getClientOriginalName());

            //filename to store
            $filenametostore = Str::slug($file_name) . '_' . uniqid() . '.' . date("YmdHi") . '.' . $extension;

            Storage::put($model->getStorageFilePath() . $filenametostore, fopen($file, 'r+'));

            Upload::create([
                'url' => $model->getFilePath() . $filenametostore,
                'nombre' => $filenametostore,
                'mime' => $file->getClientMimeType(),
                'extension' => $extension,
                'uploadable_id' => $model->id,
                'uploadable_type' => get_class($model),
                'tipo_upload_id' => $tipo_upload->id
            ]);
        }

    }
}
