<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUpload extends Model
{
    protected $table = 'tipos_uploads';
}
