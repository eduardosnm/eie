<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AsignaturaPostulacion extends Pivot
{
    protected $table = 'asignatura_postulacion';

    protected $dates = ['editando'];

    public function puntuaciones()
    {
        return $this->belongsToMany(TablaPonderacionItem::class, 'asignatura_tabla_puntajes',
            'asignatura_postulacion_id', 'tabla_item_id')
            ->withPivot('puntaje');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function puntaje()
    {
        return $this->hasMany(Puntaje::class, 'asignatura_postulacion_id');
    }

    public function cumplioElTiempo()
    {
        return $this->editando->diffInMinutes( Carbon::now() ) > 5;
    }

    public function usuarioLoEstabaEditando()
    {
        return $this->user_id == auth()->user()->id;
    }
}
