<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Puntaje extends Pivot
{
    protected $table = 'asignatura_tabla_puntajes';

    public function tabla_ponderacion()
    {
        return $this->hasOneThrough(TablaPonderacion::class, TablaPonderacionItem::class, 'id', 'id', 'tabla_item_id');
    }

    public function item()
    {
        return $this->belongsTo(TablaPonderacionItem::class, 'tabla_item_id');
    }

}
