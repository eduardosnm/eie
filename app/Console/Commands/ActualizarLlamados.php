<?php

namespace App\Console\Commands;

use App\Llamado;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class ActualizarLlamados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'llamados:actualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza es estado de los llamados a concurso';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $llamados = Llamado::whereEstado(true)->get();
        $hoy = Carbon::parse(date("Y-m-d"));
        foreach ($llamados as $llamado){
            if ($hoy->gt($llamado->fecha_fin)){
                $llamado->update(['estado'=>false]);
                Log::info("**** CRON JOB **** SE ACTUALIZO EL ESTADO DEL LLAMADO ". $llamado->id."\n");
            }
        }

        if (App::environment('local')){
            Log::info("**** EL CRON SE EJECUTO A LAS ". date("d/m/Y H:i:s")."\n");
        }
        $this->info('El comando se ejecutó exitosamente');
    }
}
