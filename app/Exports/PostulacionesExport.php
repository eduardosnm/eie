<?php

namespace App\Exports;


use App\TablaPonderacion;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class PostulacionesExport implements FromCollection, WithHeadings, WithMapping, WithEvents, ShouldAutoSize
{
    use Exportable, RegistersEventListeners;

    protected $asignatura;
    protected $tabla;
    protected $llamado;
    protected $result;
    protected $columnas;
    protected $filas;

    public function __construct($result,$asignatura, $llamado)
    {
        $this->result = $result;
        $this->asignatura = $asignatura;
        $this->llamado = $llamado;
        $this->tabla = TablaPonderacion::all();
        $this->columnas = range("A", "Z");
    }


    /**
     * @return array
     */
    public function headings(): array
    {
        $headers = $this->tabla->pluck('nombre')->toArray();
        $headers = Arr::prepend($headers, 'Correo');
        $headers = Arr::prepend($headers, 'Teléfono');
        $headers = Arr::prepend($headers, 'Postulante');
        $headers = Arr::add($headers, count($headers), 'Total');

        $this->filas = count($headers) - 1;
        return [
            [$this->asignatura->nombre],
            [],
            $headers
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->result;

    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($postulante): array
    {
        $puntajes = $postulante->getPuntaje($this->llamado->id, $this->asignatura->id)
            ->puntaje->groupBy('tabla_ponderacion_id');

        $docencia = 0;
        $investigacion = 0;
        $extension = 0;
        $gestion = 0;
        $total = 0;
        foreach ($puntajes as $index => $puntaje){
            $unico = 0;
            $pts = 0;
            foreach($puntaje as $value){
                if ($value->item->tipo == "unico"){
                    if ($unico < $value->puntaje){
                        $unico = $value->puntaje;
                    }
                }else{
                    $pts += $value->puntaje;
                }
            }

            $pts += $unico;
            $docencia += $index == 1 ? $pts : 0;
            $investigacion += $index == 2 ? $pts : 0;
            $extension += $index == 3 ? $pts : 0;
            $gestion += $index == 4 ? $pts : 0;
            $total = $docencia + $investigacion + $extension + $gestion;
        }
        return [
            $postulante->full_name,
            $postulante->telefono,
            $postulante->email,
            $docencia != 0 ? $docencia : "0",
            $investigacion != 0 ? $investigacion : "0",
            $extension != 0 ? $extension : "0",
            $gestion != 0 ? $gestion : "0",
            $total
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        $styleTitulos = [
            'font' => [
                'bold' => true,
                'size' => 12
            ]
        ];
        return [
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setCreator(config('app.name'));
            },
            AfterSheet::class => function(AfterSheet $event) use ($styleTitulos) {
                $event->sheet->getStyle("A3:{$this->columnas[$this->filas]}3")->applyFromArray($styleTitulos)
                    ->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
                $event->sheet->mergeCells("A1:{$this->columnas[$this->filas]}1");
                $event->sheet->getStyle("A1:{$this->columnas[$this->filas]}1")
                    ->applyFromArray(["font"=>["bold" => true, "size" => 15]])
                    ->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            }
        ];
    }
}
