@extends('layouts.master_postulantes')
@section('contenido')
    <style>
        .error {
            color: red;
            font-size: 1rem !important;
            position: relative;
            line-height: 1;
            width: 100%;
        }
    </style>

    <div class="col-md-12 order-md-1">
        @include('partials.errors')
        @include('partials.success')
        <div class="row mb-3">
            <div class="mx-auto text-center">
                <h3>{{ strtoupper($llamado->titulo) }}</h3>
            </div>
        </div>

        <di class="row">
            <div>
                {!! $llamado->descripcion !!}
            </div>
        </di>
        <hr>

        <div class="row">
            <div class="alert alert-danger text-center" role="alert" style="width: 100%;">
                <p>Por favor asegurese de leer el <a href="{{ asset($llamado->reglamento->url) }}" target="_blank" class="alert-link">reglamento</a> y el
                    <a href="{{ route('postulantes.manuales') }}" target="_blank" class="alert-link">instructivo</a>.</p>
                <p>Luego verifique de completar correctamente los datos ya que luego <b>NO podrán ser modificados</b>.</p>
            </div>
        </div>

        <form action="{{ route('postulantes.guardar') }}" id="formPostulante" enctype="multipart/form-data" method="POST">
            @csrf
            <input type="hidden" name="llamado_id" value="{{ $llamado->id }}">
            <fieldset>
                <legend>Datos personales</legend>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" required>

                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" id="apellido" name="apellido" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="dni">DNI</label>
                        <input type="number" class="form-control" id="dni" name="dni" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 mb-3">
                        <label for="cuil_cuit">CUIL/CUIT (sin guiones)</label>
                        <input type="number" class="form-control" id="cuil_cuit" name="cuil_cuit" required>

                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" required>

                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="lugar_nacimiento">Lugar de nacimiento</label>
                        <input type="text" class="form-control" id="lugar_nacimiento" name="lugar_nacimiento" required>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 mb-3">
                        <label for="email">Correo electrónico</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input type="email" class="form-control" id="email" name="email" required>

                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="domicilio">Domicilio</label>
                        <input type="text" class="form-control" id="domicilio" name="domicilio" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 mb-3 mx-auto">
                        <label for="telefono">Títulos de grado</label>
                        <div class="contenedor">
                            <div class="row mb-2 titulo">
                                <div class="col-md-10">
                                    <input type="text" class="form-control nombre" name="titulos_grado[]" placeholder="Título">
                                </div>
                                <div class="col-md-2">
                                    <i class="fas fa-plus-circle fa-2x cursor agregar_campo" style="color: green"></i>
                                    <i class="fas fa-minus-circle fa-2x cursor eliminar" style="color: red; display: none"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="row">
                <div class="alert alert-danger text-center" role="alert" style="width: 100%;">
                    <h5>Seleccione una o mas asignaturas a las que se desea postular</h5>
                </div>
            </div>

            <fieldset>
                <legend>Asignaturas</legend>
                <div class="row">
                    @include('postulantes._carreras', ["carreras" => $carreras])
                </div>
            </fieldset>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="">Adjuntar CV</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file">
                        <label class="custom-file-label" data-browse="Seleccionar archivo" for="file">Archivo en formato PDF de hasta 2MB</label>
                    </div>
                </div>
            </div>

            <div class="form-row">
                {!! NoCaptcha::display(["data-callback" => "enableBtn"]) !!}
            </div>

            <div class="form-row">
                <div class="mx-auto">
                    <button type="submit" disabled class="btn btn-success btn-lg finalizar">Finalizar</button>
                </div>

            </div>
        </form>
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            function enableBtn() {
                $(".finalizar").removeAttr('disabled');
            }
            window.enableBtn = enableBtn;
            var datos_personales_rules = {
                ignore: [],
                rules: {
                    nombre: "required",
                    apellido: "required",
                    dni: {
                        required: true,
                        number: true,
                        minlength: 7,
                        maxlength: 8
                    },
                    cuil_cuit: {
                        required: true,
                        // regex: "/^(20|23|24|27|30|33|34)[0-9]{8}[0-9]$/",
                    },
                    fecha_nacimiento: "required",
                    lugar_nacimiento: "required",
                    domicilio: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    telefono: "required",
                    llamado_id: "required",
                    'titulos_grado[]': {
                        required: true
                    },
                    'asignaturas[]': {
                        required: true
                    },
                    file: {
                        required: true,
                        extension: "pdf",
                        accept:"application/pdf",
                        maxsize: 2000000,
                    }
                },
                messages: {
                    nombre: "Complete el campo nombre",
                    apellido: "Complete el campo apellido",
                    dni: {
                        required: "Complete el campo dni",
                        number: "El campo DNI solo puede contener numeros",
                        minlength: "El campo DNI permite un minimo de 7 digitos y un maximo de 8",
                        maxlength: "El campo DNI permite un minimo de 7 digitos y un maximo de 8"
                    },
                    cuil_cuit: {
                        required: "Complete el campo CUIL/CUIT",
                        regex: "Formato no válido"
                    },
                    fecha_nacimiento: "Complete el campo fecha de nacimiento",
                    lugar_nacimiento: "Complete el campo lugar de nacimiento",
                    domicilio: "Complete el campo domicilio",
                    email: {
                        required: "Complete el campo correo electrónico",
                        email: "Proporcione un formato de mail valido"
                    },
                    telefono: "Complete el campo telefono",
                    llamado_id : "Debe seleccionar una opcion.",
                    'titulos_grado[]': {
                        required: "Complete el nombre del titulo"
                    },
                    'asignaturas[]': {
                        required: "Debe seleccionar al menos una asignatura"
                    },
                    file:{
                        required: "No olvide adjuntar su cv.",
                        extension: "Solo se permiten archivos en formato PDF",
                        maxsize: "El tamaño maximo permitido es de 2MB"
                    }
                }
            };

            $('#file').change(function(){
                var fileName = $(this).val();
                $(this).next('.custom-file-label').html(fileName);
            });

            $("#formPostulante").validate(datos_personales_rules);

            $(document).on("click", ".agregar_campo", function (e) {
                e.preventDefault();
                var evaluador = $(this).closest('.titulo').clone(true,true);
                var contenedor = $(this).closest('.contenedor');
                $(evaluador).find('.agregar_campo').remove();
                $(evaluador).find('.nombre').val(null);
                $(evaluador).find('.eliminar').show();
                $(contenedor).append(evaluador);
            });

            $(document).on("click", ".eliminar", function (e) {
                console.log($(this));
                $(this).closest('.titulo').remove();
            });

            $(document).on('wheel', 'input[type=number]', function(e){
                return false;
            });

        })
    </script>
@endsection
