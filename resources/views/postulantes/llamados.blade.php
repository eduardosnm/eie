@extends('layouts.master_postulantes')
@section('contenido')
    <div class="col-md-12 order-md-1">
        <div class="row">
            <div class="mx-auto">
                <h1>Llamados activos</h1>
            </div>
        </div>
        @include('partials.errors')
        @include('partials.success')
        <div class="row">
            @forelse($llamados as $llamado)
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">{{ $llamado->titulo }}</h6>
                        </div>
                        <div class="card-body">
                            <div class="modal-body">
                                <p>Fecha de inicio: {{ $llamado->fecha_inicio->format("d/m/Y") }}</p>
                                <p>Fecha de cierre: {{ $llamado->fecha_fin->format("d/m/Y") }}</p>
                                @if($llamado->reglamento)
                                    <a href="{{ asset($llamado->reglamento->url) }}" target="_blank"><small>Reglamento</small></a>
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="mx-auto">
                                    @if($llamado->fecha_inicio->lessThan($hoy))
                                    <a href="{{ route('postulantes.carreras', $llamado) }}" target="_blank" class="btn btn-success btn-icon-split">
                                        <span class="icon text-white-50"><i class="fas fa-sign-in-alt"></i></span>
                                        <span class="text">Inscribirse</span>
                                    </a>
                                    @else
                                        <p><b>Usted se podrá inscribir a partir del dia {{ $llamado->fecha_inicio->format("d/m/Y") }}</b></p>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            @empty
                <p>No se encontraron llamados activos</p>
            @endforelse
        </div>
    </div>
@endsection

