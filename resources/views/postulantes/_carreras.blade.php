@forelse($carreras as $nombre => $asignaturas)
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $nombre }}</h6>
            </div>
            <div class="card-body">
                <div class="modal-body">
                    @forelse($asignaturas as $asignatura)
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input asignaturas" name="asignaturas[]" value="{{ $asignatura->id }}" id="customCheck{{ $asignatura->id }}">
                            <label class="custom-control-label" for="customCheck{{ $asignatura->id }}">{{ $asignatura->nombre }}</label>
                        </div>
                    @empty
                        <p>No se encontraron asignaturas</p>
                    @endforelse
                </div>

            </div>
        </div>
    </div>
@empty
    <h5>No hay carreras cargadas</h5>
@endforelse
