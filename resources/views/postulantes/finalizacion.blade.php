@extends('layouts.master_postulantes')
@section('contenido')
    <div class="col-md-12 order-md-1">
        <div class="col-md-5 col-sm-5 col-xs-12 mx-auto">
            <div class="card shadow mb-4">
                <div class="card-header py-3 text-center">
                    <h6 class="m-0 font-weight-bold text-primary">Comprobante</h6>
                </div>
                <div class="card-body">
                    <p>Su inscripción a la "CONVOCATORIA DE ASPIRANTES PARA CUBRIR CONTRATOS DE SERVICOS DOCENTES EVENTUALES EN LA EIE" se realizo exitosamente.</p>
                    <p>Apellido y nombre: {{ ucwords($postulacion->postulante->full_name) }}</p>
                    <p>Fecha: {{ $postulacion->created_at->format("d/m/Y H:i:s") }}</p>
                    <p>Numero: {{ $postulacion->nro_comprobante }}</p>
                    <p>Llamado: {{ $postulacion->llamado->titulo }}</p>
                    <p>Asignaturas: </p>
                    <ul>
                        @foreach($postulacion->asignaturas as $asignatura)
                            <li>{{ $asignatura->carrera->nombre }} - {{ $asignatura->nombre }}</li>
                        @endforeach
                    </ul>

                    <div class="row">
                        <div class="mx-auto">
                            <a href="{{ route('postulantes.imprimir', $postulacion->uuid) }}" target="_blank" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50"><i class="fas fa-print"></i></span>
                                <span class="text">Imprimir comprobante</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
