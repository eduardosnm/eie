<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{ $postulacion->nro_comprobante }}</title>

</head>
<body>
    <div style="width: 100%; padding-left: 1%;border: 1px solid;">
        <p>Su inscripción a la "CONVOCATORIA DE ASPIRANTES PARA CUBRIR CONTRATOS DE SERVICOS DOCENTES EVENTUALES EN LA EIE" se realizo exitosamente.</p>
        <p>Apellido y nombre: {{ ucwords($postulacion->postulante->full_name) }}</p>
        <p>Fecha: {{ $postulacion->created_at->format("d/m/Y H:i:s") }}</p>
        <p>Numero: {{ $postulacion->nro_comprobante }}</p>
        <p>Llamado: {{ $postulacion->llamado->titulo }}</p>
        <p>Asignaturas: </p>
        <ul>
            @foreach($postulacion->asignaturas as $asignatura)
                <li>{{ $asignatura->carrera->nombre }} - {{ $asignatura->nombre }}</li>
            @endforeach
        </ul>
    </div>
</body>
</html>
