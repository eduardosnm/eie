@if(session()->has('success'))
<div class="alert bg-gradient-success" role="alert" style="color: white">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<strong>
		<i class="fa fa-check-circle fa-lg fa-fw"></i> Éxito. &nbsp;
	</strong>
	{{Session::get('success')}}
</div>
@endif
