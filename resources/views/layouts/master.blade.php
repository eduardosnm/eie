<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ config('app.name') }}</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset(mix('/css/app.css')) }}" rel="stylesheet">

</head>

<body id="page-top">
<style>
    .blanco {
        color: white !important;
    }

    .cursor {
        cursor: pointer;
    }

    .prohibido {
        cursor: not-allowed;
    }
</style>
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
            <div class="sidebar-brand-icon">
                <i class="fas fa-graduation-cap"></i>
            </div>
            <div class="sidebar-brand-text mx-3">EIE</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
{{--        <li class="nav-item active">--}}
{{--            <a class="nav-link" href="">--}}
{{--                <i class="fas fa-fw fa-tachometer-alt"></i>--}}
{{--                <span>Inicio</span></a>--}}
{{--        </li>--}}

        <!-- Divider -->
{{--        <hr class="sidebar-divider">--}}

        <!-- Heading -->
        <div class="sidebar-heading">
            Menú
        </div>

        @can('listado-usuarios')
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('usuarios.listado') }}">
                <i class="fas fa-user-friends"></i>
                <span>Usuarios</span>
            </a>
        </li>
        @endcan

        @can('listado-asignaturas')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCarreras"
               aria-expanded="true" aria-controls="collapseCarreras">
                <i class="fas fa-book"></i>
                <span>Carreras</span>
            </a>
            <div id="collapseCarreras" class="collapse" aria-labelledby="headingCarreras" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('carreras.listado') }}">Administrar carreras</a>
                    <a class="collapse-item" href="{{ route('asignaturas.listado') }}">Administrar asignaturas</a>
                </div>
            </div>
        </li>
        @endcan

        @can('listado-evaluadores')
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('evaluadores.listado') }}">
                <i class="fas fa-chalkboard-teacher"></i>
                <span>Evaluadores</span>
            </a>
        </li>
        @endcan

{{--        @can('listado-tabla_ponderacion')--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link collapsed" href="{{ route('tabla.listado') }}">--}}
{{--                    <i class="fas fa-table"></i>--}}
{{--                    <span>Tabla de ponderación</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endcan--}}

        @can('listado-tribunal')
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('tribunales.crear') }}">
                <i class="fas fa-users"></i>
                <span>Designar tribunal</span>
            </a>
        </li>
        @endcan

        @can('listado-postulantes')
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('postulaciones.listado') }}">
                <i class="fas fa-user-check"></i>
                <span>Postulantes</span>
            </a>
        </li>
        @endcan

        @can('listado-llamados')
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('llamados.listado') }}">
                <i class="fas fa-bell"></i>
                <span>Administrar llamados</span>
            </a>
        </li>
        @endcan

        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('usuarios.manuales') }}" target="_blank">
                <i class="fas fa-question-circle"></i>
                <span>Ayuda</span>
            </a>
        </li>


        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>


                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->full_name }}</span>
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">({{ implode(",", auth()->user()->getRoles()->toArray()) }})</span>
                            <i class="fas fa-user fa-2x"></i>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Salir
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">
                        @yield('titulo')
                    </h1>
                </div>

                @yield('contenido')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; EIE {{ date("Y") }}</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>

<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Esta seguro que desea cerrar su sesión?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-footer">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Cerrar sesión</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{ asset(mix('/js/app.js')) }}"></script>
@yield('js')

</body>

</html>
