@extends('layouts.master')
@section('titulo', 'Ver evaluador')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Evaluador</h6>

                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">Apellido y nombre</label>
                                <p>{{ $evaluador->full_name}}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">Correo electrónico</label>
                                <p>{{ $evaluador->email }}</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="h4 mb-0 text-gray-800">Carreras del cuales forma parte del tribunal</h5>
    </div>
    @forelse($llamados as $nombre => $tribunales)
        <h5 class="h5 mb-0 text-gray-800">{{ $nombre }}</h5>

        <div class="row">
            @foreach($tribunales as $tribunal)
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">{{ $tribunal->carrera->nombre }}</h6>
                        </div>
                        <div class="card-body">
                            <div class="modal-body">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input seleccionar_todos">
                                </div>
                                <ul>
                                    @forelse($tribunal->carrera->asignaturas as $asignatura)
                                        <li>{{ $asignatura->nombre }}</li>
                                    @empty
                                        <p>No se encontraron asignaturas</p>
                                    @endforelse
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @empty
        <h5>No hay carreras cargadas</h5>
    @endforelse


@endsection

