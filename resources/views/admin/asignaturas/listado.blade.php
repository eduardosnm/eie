@extends('layouts.master')
@section('titulo', 'Listado de asignaturas')
@section('contenido')
    @include('partials.errors')
    @include('partials.success')
    <div class="row" style="margin-bottom: 1%">
        <div class="col">
            <a href="{{ route('asignaturas.crear') }}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-plus"></i>
                    </span>
                <span class="text">Nueva asignatura</span>
            </a>
        </div>
        <div class="col-md-3">
            <form action="{{ route('asignaturas.listado') }}" method="GET">
                <input type="text" class="form-control" value="{{ request('buscar') }}" name="buscar" placeholder="Buscar carrera">
            </form>
        </div>
        <div class="col-md-1">
            <a href="{{ route('asignaturas.listado') }}" title="Limpiar resultados">
                <i class="fas fa-broom fa-2x"></i>
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <th>#</th>
            <th>Nombre</th>
            <th>Estado</th>
            <th>Carrera</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @forelse($asignaturas as $asignatura)
                <tr>
                    <td>{{ $asignatura->id }}</td>
                    <td>{{ $asignatura->nombre }}</td>
                    <td>{!! $asignatura->estado_label !!}</td>
                    <td>{{ $asignatura->carrera->nombre }}</td>
                    <td>
                        <a href="{{ route('asignaturas.editar', $asignatura->id) }}" class="btn btn-warning btn-circle btn-sm editar" title="Editar asignatura">
                            <i class="far fa-edit"></i>
                        </a>

                        <span class="btn btn-danger btn-circle btn-sm eliminar cursor" title="Eliminar"
                              data-url="{{route('asignaturas.eliminar', $asignatura->id)}}">
                            <i class="far fa-trash-alt"></i>
                        </span>
                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        <div class="modal fade" id="eliminarCarrera" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar la carrera?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4><span style="color: red; font-weight: bold">¡ADVERTENCIA!</span> Si elimina la carrera se eliminaran las asignaturas pertenecientes a esta.</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <form method="POST" id="formEliminarCarrera">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="eliminarCarrera" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar la asignatura?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <form method="POST" id="formEliminarCarrera">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        {{ $asignaturas->appends([
            'buscar' => request('buscar')
        ])->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            $(".eliminar").click(function () {
                var url = $(this).data("url");
                $("#formEliminarCarrera").attr("action", url);
                $("#eliminarCarrera").modal('show');
            });
        });
    </script>
@endsection
