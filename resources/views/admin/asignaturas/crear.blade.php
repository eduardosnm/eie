@extends('layouts.master')
@section('titulo', 'Nueva asignatura')
@section('contenido')
    @include('partials.errors')
    @include('partials.success')

    <form action="{{ route('asignaturas.guardar') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Carrera</h6>
                    </div>
                    <div class="card-body">

                        <div class="form-group row">
                            <select name="carrera_id" class="form-control" required>
                                <option disabled selected>Seleccione una carrera</option>
                                @foreach($carreras as $carrera)
                                    <option value="{{ $carrera->id }}" @if($carrera->id == old('carrera_id')) selected @endif>{{ $carrera->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="pb-3 mx-auto">
                <button class="btn btn-primary" id="crear_asignatura">Nueva asignatura</button>
            </div>
        </div>

        <div class="row" id="tarjetero">
            <div class="col-md-3 tarjeta">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Asignatura <span class="numero">1</span></h6>
                        <div class="dropdown no-arrow" style="display: none">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item eliminar">Eliminar</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="carrera[nombre]">Nombre de la asignatura</label>
                            <input type="input" class="form-control asignatura_nombre" value="{{ old('asignatura')[1] }}" required name="asignatura[1]" placeholder="Nombre">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mx-auto">
                <button class="btn btn-success btn-lg" type="submit">Guardar</button>
            </div>

        </div>
    </form>
    @php
        $asignaturas_old = '';
        if (!is_null(old('asignatura'))){
            $asignaturas_old = old('asignatura');
            unset($asignaturas_old[1]);
        }
    @endphp
@endsection
@section('js')
    <script>
        $(function(){
            $("#crear_asignatura").click(function(e){
                e.preventDefault();
                var cantidad_tarjetas = $(".tarjeta").length;
                var tarjeta = $(".tarjeta:first").clone(true);
                tarjeta.find('.asignatura_nombre')
                    .attr("name", "asignatura["+(++cantidad_tarjetas)+"]")
                    .prop("required", true)
                    .val(null);
                tarjeta.find('.numero')
                    .html(cantidad_tarjetas);
                tarjeta.find('.dropdown')
                    .show();
                $("#tarjetero").append(tarjeta);
            });

            $(document).on("click", ".eliminar", function () {
                $(this).closest(".tarjeta").remove();
            });

            var asignaturas = JSON.parse('<?php echo json_encode($asignaturas_old) ?>');
            if (asignaturas != ''){
                $.each(asignaturas, function (i, item) {
                    var cantidad_tarjetas = $(".tarjeta").length;
                    var tarjeta = $(".tarjeta:first").clone(true);
                    tarjeta.find('.asignatura_nombre')
                        .attr("name", "asignatura["+(++cantidad_tarjetas)+"]")
                        .prop("required", true)
                        .val(item);
                    tarjeta.find('.numero')
                        .html(cantidad_tarjetas);
                    tarjeta.find('.dropdown')
                        .show();
                    $("#tarjetero").append(tarjeta);
                })
            }

        });
    </script>
@endsection
