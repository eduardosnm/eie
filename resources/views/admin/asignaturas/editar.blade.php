@extends('layouts.master')
@section('titulo', 'Editar asignatura')
@section('contenido')
    @include('partials.errors')
    @include('partials.success')

    <form action="{{ route('asignaturas.actualizar', $asignatura) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Carrera</h6>
                    </div>
                    <div class="card-body">

                        <div class="form-group row">
                            <select name="carrera_id" class="form-control" required>
                                <option disabled selected>Seleccione una carrera</option>
                                @foreach($carreras as $carrera)
                                    <option value="{{ $carrera->id }}" @if($carrera->id == $asignatura->carrera->id) selected @endif>{{ $carrera->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Asignatura</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="custom-control custom-switch">
                                <input type="input" class="form-control" id="nombre" value="{{ $asignatura->nombre }}" required name="nombre" placeholder="Nombre">
                                <input type="hidden" name="asignatura_id" value="{{ $asignatura->id }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Estado de la asignatura</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" @if($asignatura->estado) checked @endif class="custom-control-input"
                                       id="estado_checkbox">
                                <label class="custom-control-label" for="estado_checkbox">Activo</label>
                                <input type="hidden" id="estado" name="estado" value="{{ $asignatura->estado ? 1 : 0 }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mx-auto">
                <button class="btn btn-success btn-lg" type="submit">Guardar</button>
            </div>

        </div>
    </form>

@endsection
@section('js')
    <script>
        $(function(){
            $("#estado_checkbox").change(function () {
               if ($(this).is(":checked")){
                   $("#estado").val(1);
               }else{
                   $("#estado").val(0);
               }
            });
        });
    </script>
@endsection
