@foreach($carreras as $key =>$carrera)
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $carrera->nombre }}</h6>
                <input type="hidden" name="carrera[{{ $key }}][id]" value="{{ $carrera->id }}">
            </div>
            <div class="card-body">
                <div class="contenedor">
                    <div class="row mb-2 evaluador">
                        <div class="col-md-10">
                            <input type="text" class="form-control nombre" required placeholder="Nombre del evaluador">
                            <input type="hidden" class="evaluador_id" required name="carrera[{{ $key }}][evaluador][0][id]">
                        </div>
                        <div class="col-md-2">
                            <i class="fas fa-plus-circle fa-2x cursor agregar_campo" style="color: green"></i>
                            <i class="fas fa-minus-circle fa-2x cursor eliminar" style="color: red; display: none"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach
<script>
    $(function () {
        var evaluadores = <?= $evaluadores; ?>;

        $("input.nombre").autocomplete({
            source: evaluadores,
            select: function (event, ui) {
                $(this).next('.evaluador_id').val(ui.item.id);
            }
        });
    });
</script>
