@extends('layouts.master')
@section('titulo', 'Asignar evaluadores a las carreras')
@section('contenido')

    @include('partials.errors')
    <form action="{{ route('tribunales.guardar') }}" method="POST">
        @csrf
        <div class="row mb-3">
            <div class="mx-auto">
                <select name="llamado_id" id="llamado" class="form-control">
                    <option disabled selected>Seleccionar un llamado</option>
                    @forelse($llamados as $llamado)
                        <option value="{{ $llamado->id }}">{{ $llamado->titulo }}</option>
                        @empty
                        <option disabled>No se encontraron llamados activos</option>
                    @endforelse
                </select>
            </div>
        </div>

        <div class="row carreras">

        </div>
        <div class="row guardar" style="display: none">
            <div class="mx-auto">
                <button class="btn btn-success btn-lg" type="submit">Guardar</button>
            </div>
        </div>
    </form>
@endsection
@section('js')
    <script>
        $(function () {
            var evaluadores = <?= $evaluadores; ?>;

            $("input.nombre").autocomplete({
                source: evaluadores,
                select: function (event, ui) {
                    $(this).next('.evaluador_id').val(ui.item.id);
                }
            });

            $(document).on("click", ".agregar_campo", function (e) {
                e.preventDefault();
                var evaluador = $(this).closest('.evaluador').clone(true,true);
                var contenedor = $(this).closest('.contenedor');
                var name = $(evaluador).find('.evaluador_id').attr("name");
                var numbers = name.match(/\d+/g).map(Number);
                $(evaluador).find('.evaluador_id').attr('name', 'carrera['+numbers[0]+'][evaluador]['+$(contenedor).find('.evaluador').length+'][id]').val(null);
                $(evaluador).find('input.nombre').remove();
                $(evaluador).find('.agregar_campo').remove();
                $(evaluador).find('.eliminar').show();
                $(contenedor).append(evaluador);
                $('<input type="text" class="form-control nombre" required placeholder="Nombre del evaluador"/>')
                    .autocomplete({
                        source: evaluadores,
                        select: function (event, ui) {
                            $(this).next('.evaluador_id').val(ui.item.id);
                        }
                    }).insertBefore($(contenedor).find('.evaluador:last').find('input.evaluador_id'));
            });

            $(document).on("click", ".eliminar", function (e) {
                console.log($(this));
                $(this).closest('.evaluador').remove();
            });

            $("#llamado").change(function () {
                var llamado_id = $(this).val();

                if (!llamado_id){
                    return;
                }
                var url = '{{ url('tribunal/getCarreras/') }}' + '/' + llamado_id;

                $.ajax({
                    url: url,
                    dataType: 'html'
                }).done(function (result) {
                    $(".carreras").html(result);
                    $(".guardar").show();
                }).fail(function (result) {
                    console.log(result);
                    alert("Ocurrio un error inesperado")
                })
            });

            $("#llamado").change();
        });
    </script>
@endsection
