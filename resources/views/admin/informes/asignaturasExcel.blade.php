@extends('layouts.master')
@section('titulo', 'Imprimir listados de puntuaciones por asignatura')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <form action="{{ route('informes.generarExcel', $llamado) }}" method="POST" style="width: 100%">
        @csrf
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Llamado</h6>

                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col">
                                    <label class="font-weight-bold">TÍTULO</label>
                                    <p>{{ $llamado->titulo }}</p>
                                </div>

                                <div class="col">
                                    <label class="font-weight-bold">FECHA DE INICIO</label>
                                    <p>{{ $llamado->fecha_inicio->format("d/m/Y") }}</p>
                                </div>

                                <div class="col">
                                    <label class="font-weight-bold">FECHA DE FINALIZACION</label>
                                    <p>{{ $llamado->fecha_fin->format("d/m/Y") }}</p>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <label class="font-weight-bold">DESCRIPCIÓN</label>
                                    {!! $llamado->descripcion !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h3 class="h4 mb-0 text-gray-800">Seleccione las asignaturas de las cuales quiere imprimir el listado de puntuaciones</h3>
        </div>

        <div class="row">
            @forelse($result as $nombre => $asignaturas)
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">{{ $nombre }}</h6>
                        </div>
                        <div class="card-body">
                            <div class="modal-body">

                                @forelse($asignaturas as $asignatura)
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="asignaturas[]" value="{{$asignatura->id}}"
                                               id="asignatura{{ \Illuminate\Support\Str::slug($nombre) . "_".$asignatura->id }}">
                                        <label class="custom-control-label" for="asignatura{{ \Illuminate\Support\Str::slug($nombre) . "_".$asignatura->id }}">{{ $asignatura->nombre }}</label>
                                    </div>
                                @empty
                                    <p>No se encontraron asignaturas</p>
                                @endforelse
                            </div>

                        </div>
                    </div>
                </div>
            @empty
                <h5>No hay carreras cargadas</h5>
            @endforelse
        </div>

        <div class="row">
            <div class="mx-auto">
                <button type="submit" id="btn-submit" class="btn btn-success">Generar XLS</button>
            </div>
        </div>
    </form>

@endsection
@section('js')
    <script>
        $(function () {

        });
    </script>
@endsection
