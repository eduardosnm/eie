@extends('layouts.master')
@section('titulo', 'Ver llamado')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Llamado</h6>

                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">TÍTULO</label>
                                <p>{{ $llamado->titulo }}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">FECHA DE INICIO</label>
                                <p>{{ $llamado->fecha_inicio->format("d/m/Y") }}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">FECHA DE FINALIZACION</label>
                                <p>{{ $llamado->fecha_fin->format("d/m/Y") }}</p>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">DESCRIPCIÓN</label>
                                <p>{!! $llamado->descripcion !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="h4 mb-0 text-gray-800">Asignaturas seleccionadas para el llamado actual</h3>
    </div>

    <div class="row">
        @forelse($result as $nombre => $asignaturas)
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">{{ $nombre }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input seleccionar_todos">
                            </div>
                            <ul>
                            @forelse($asignaturas as $asignatura)
                                <li>{{ $asignatura->nombre }}</li>
                            @empty
                                <p>No se encontraron asignaturas</p>
                            @endforelse
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        @empty
            <h5>No hay carreras cargadas</h5>
        @endforelse
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="h4 mb-0 text-gray-800">Evaluadores designados para cada carrera</h3>
    </div>

    <div class="row">
        @forelse($carreras as $nombre => $tribunales)

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">{{ $nombre }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input seleccionar_todos">
                            </div>
                            <ul>
                                @forelse($tribunales as $tribunal)
                                    <li>{{ $tribunal->evaluador->full_name }}</li>
                                @empty
                                    <p>No se encontraron asignaturas</p>
                                @endforelse
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        @empty
            <h5>No hay carreras cargadas</h5>
        @endforelse
    </div>


@endsection
@section('js')
    <script>
        $(function () {
            $(".fecha_inicio").change(function () {
                var fecha_inicio = moment($(this).val());
                var fecha_fin = fecha_inicio.add(1,"days").format("YYYY-MM-DD");

                $(this).closest('div.container-fluid').find('.fecha_fin').attr("min", fecha_fin).val(fecha_fin);
            });

            $(".seleccionar_todos").click(function () {
                var $modal_body = $(this).closest('.modal-body');
                $modal_body.find("input[type=checkbox]").prop("checked",$(this).is(":checked"));
            })
        });
    </script>
@endsection
