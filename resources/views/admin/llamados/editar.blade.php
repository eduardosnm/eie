@extends('layouts.master')
@section('titulo', 'Editar llamado')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <form action="{{ route('llamados.actualizar', $llamado) }}" method="POST" style="width: 100%" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Llamados</h6>

                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="titulo">Título</label>
                                        <input type="text" class="form-control" required id="titulo" name="titulo" value="{{ $llamado->titulo }}" placeholder="Título">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="fecha_inicio">Fecha de inicio</label>
                                        <input type="date" class="form-control fecha_inicio" id="fecha_inicio" required value="{{ $llamado->fecha_inicio->format("Y-m-d") }}"
                                               name="fecha_inicio" placeholder="Fecha de inicio" min="{{ date("Y-m-d") }}">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="fecha_fin">Fecha de finalización</label>
                                        <input type="date" class="form-control fecha_fin" id="fecha_fin" required value="{{ $llamado->fecha_fin->format("Y-m-d") }}" name="fecha_fin" placeholder="Fecha de finalización ">
                                    </div>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Adjuntar reglamento PDF</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input file" name="file[reglamento]">
                                            <label class="custom-file-label" data-browse="Buscar" for="file">
                                                <small>@if(!is_null($llamado->reglamento)) {{ substr($llamado->reglamento->nombre, 0, 40) }}
                                                    @else
                                                        PDF de hasta 20MB
                                                    @endif
                                                </small>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Adjuntar tabla de ponderacion PDF</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input file" name="file[tabla]">
                                            <label class="custom-file-label" data-browse="Buscar" for="file">
                                                <small>@if(!is_null($llamado->tabla)) {{ substr($llamado->tabla->nombre, 0, 40) }}
                                                    @else
                                                        PDF de hasta 20MB
                                                    @endif
                                                </small>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="descripcion">Descripción</label>
                                        <textarea name="descripcion" id="descripcion" class="form-control" cols="30" rows="5" placeholder="Escriba una descripción">{{ $llamado->descripcion }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h3 class="h4 mb-0 text-gray-800">Seleccione las asignaturas que estarán disponibles para este llamado</h3>
        </div>

        <div class="row">
            @forelse($carreras as $carrera)
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">{{ $carrera->nombre }}</h6>
                        </div>
                        <div class="card-body">
                            <div class="modal-body">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input seleccionar_todos" id="asignatura{{ $carrera->id . "_0" }}">
                                    <label class="custom-control-label" for="asignatura{{ $carrera->id . "_0"}}">Seleccionar todas las asignaturas</label>
                                </div>
                                @forelse($carrera->asignaturas as $asignatura)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="asignaturas[]" value="{{$asignatura->id}}"
                                               @if($asignatura->llamados()->find($llamado->id)) checked @endif id="asignatura{{ $carrera->id . "_".$asignatura->id }}">
                                        <label class="custom-control-label" for="asignatura{{ $carrera->id . "_".$asignatura->id }}">{{ $asignatura->nombre }}</label>
                                    </div>
                                @empty
                                    <p>No se encontraron asignaturas</p>
                                @endforelse
                            </div>

                        </div>
                    </div>
                </div>
            @empty
                <h5>No hay carreras cargadas</h5>
            @endforelse
        </div>

        <div class="row">
        <div class="mx-auto">
            <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
    </form>

@endsection
@section('js')
    <script src="{{ asset('tinymce/tinymce.js') }}"></script>
    <script>
        $(function () {
            tinymce.init({
                selector: "#descripcion",
                language: 'es_MX',
                height: 300,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            });
            $(".fecha_inicio").change(function () {
                var fecha_inicio = moment($(this).val());
                var fecha_fin = fecha_inicio.add(1,"days").format("YYYY-MM-DD");

                $(this).closest('div.container-fluid').find('.fecha_fin').attr("min", fecha_fin).val(fecha_fin);
            });

            $(".seleccionar_todos").click(function () {
                var $modal_body = $(this).closest('.modal-body');
                $modal_body.find("input[type=checkbox]").prop("checked",$(this).is(":checked"));
            });

            $('.file').on('change',function(){
                var fileName = $(this).val();
                $(this).next('.custom-file-label').html(fileName.substring(0, 20));
            });
        });
    </script>
@endsection
