@extends('layouts.master')
@section('titulo', 'Editar tribunal')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <form action="{{ route('llamados.actualizarTribunal', $llamado) }}" method="POST" style="width: 100%" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            @forelse($carreras as $nombre => $tribunales)
                @php $nombre = explode("_",$nombre)@endphp
                <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">{{ $nombre[0] }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            <div class="contenedor">
                                @forelse($tribunales as $nro => $tribunal)
                                <div class="row mb-2 evaluador">
                                    <div class="col-md-10">
                                        <input type="text" class="form-control nombre" required placeholder="Nombre del evaluador" value="{{ $tribunal->evaluador->full_name }}">
                                        <input type="hidden" class="evaluador_id" required
                                               value="{{ $tribunal->evaluador->id }}" name="carrera[{{ $nombre[1] }}][evaluador][{{ $nro }}]">
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fas fa-plus-circle fa-2x cursor agregar_campo" style="color: green; @if(!$loop->first) display:none;@endif"></i>
                                        <i class="fas fa-minus-circle fa-2x cursor eliminar" style="color: red; @if($loop->first) display:none;@endif"></i>

                                    </div>
                                </div>
                                    @empty
                                    <p>No se encontraron resultados</p>
                                @endforelse
                            </div>
                        </div>

                    </div>
                </div>
            </div>
                @empty
                <h5>No se encontro tribunal asignado para esta materia</h5>
            @endforelse
        </div>

        <div class="row">
            <div class="mx-auto">
                <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>

@endsection
@section('js')
    <script>
        $(function () {
            var evaluadores = <?= $evaluadores; ?>;

            $("input.nombre").autocomplete({
                source: evaluadores,
                select: function (event, ui) {
                    $(this).next('.evaluador_id').val(ui.item.id);
                }
            });

            $(document).on("click", ".agregar_campo", function (e) {
                e.preventDefault();
                var evaluador = $(this).closest('.evaluador').clone(true,true);
                var contenedor = $(this).closest('.contenedor');
                var name = $(evaluador).find('.evaluador_id').attr("name");
                var numbers = name.match(/\d+/g).map(Number);
                $(evaluador).find('.evaluador_id').attr('name', 'carrera['+numbers[0]+'][evaluador]['+$(contenedor).find('.evaluador').length+']').val(null);
                $(evaluador).find('input.nombre').remove();
                $(evaluador).find('.agregar_campo').remove();
                $(evaluador).find('.eliminar').show();
                $(contenedor).append(evaluador);
                $('<input type="text" class="form-control nombre" required placeholder="Nombre del evaluador"/>')
                    .autocomplete({
                        source: evaluadores,
                        select: function (event, ui) {
                            $(this).next('.evaluador_id').val(ui.item.id);
                        }
                    }).insertBefore($(contenedor).find('.evaluador:last').find('input.evaluador_id'));
            });

            $(document).on("click", ".eliminar", function (e) {
                $(this).closest('.evaluador').remove();
            });
        });
    </script>
@endsection
