@extends('layouts.master')
@section('titulo', 'Listado de llamados')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <div class="row" style="margin-bottom: 1%">
        <div class="col">
            <a href="{{ route('llamados.crear') }}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-plus"></i>
                    </span>
                <span class="text">Nuevo llamado</span>
            </a>

            <a href="{{ route('tribunales.crear') }}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-users"></i>
                    </span>
                <span class="text">Designar tribunal</span>
            </a>
        </div>
        <div class="col-md-3">
            <form action="{{ route('llamados.listado') }}" method="GET" id="buscarUsuarioForm">
                <input type="text" class="form-control" value="{{ request('buscar') }}" name="buscar" id="buscarUsuario" placeholder="Buscar llamado">
            </form>
        </div>
        <div class="col-md-1">
            <a href="{{ route('usuarios.listado') }}" title="Limpiar resultados">
                <i class="fas fa-broom fa-2x"></i>
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <th>#</th>
            <th>Título</th>
            <th>Estado</th>
            <th>Fecha de inicio</th>
            <th>Fecha de finalizacion</th>
            <th>Usuario</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @forelse($llamados as $llamado)
                <tr>
                    <td>{{ $llamado->id }}</td>
                    <td>{{ $llamado->titulo }}</td>
                    <td>{!! $llamado->estado_label !!}</td>
                    <td>{{ $llamado->fecha_inicio->format("d/m/Y") }}</td>
                    <td>{{ $llamado->fecha_fin->format("d/m/Y") }}</td>
                    <td>{{ $llamado->usuario->full_name }}</td>
                    <td>
                        <a href="{{ route('llamados.ver', $llamado) }}" class="btn btn-primary btn-circle btn-sm" title="Detalle">
                            <i class="fas fa-eye"></i>
                        </a>

                        <a href="{{ route('llamados.editar', $llamado) }}" class="btn btn-warning btn-circle btn-sm" title="Editar llamado">
                            <i class="far fa-edit"></i>
                        </a>

                        <a href="{{ route('llamados.editarTribunal', $llamado) }}" class="btn btn-success btn-circle btn-sm" title="Editar tribunal">
                            <i class="fas fa-users-cog"></i>
                        </a>

                        <span href="{{ route('llamados.editar', $llamado) }}" class="btn btn-danger btn-circle btn-sm eliminar cursor" title="Eliminar"
                              data-url="{{route('llamados.eliminar', $llamado->id)}}">
                            <i class="far fa-trash-alt"></i>
                        </span>

                        <a href="{{ route('informes.listado', $llamado) }}" class="btn btn-info btn-circle btn-sm" title="Imprimir inscriptos">
                            <i class="fas fa-file-pdf"></i>
                        </a>

                        <a href="{{ route('informes.listadoExcel', $llamado) }}" class="btn btn-secondary btn-circle btn-sm" title="Generar excel con puntuaciones">
                            <i class="fas fa-file-excel"></i>
                        </a>

                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        <div class="modal fade" id="eliminarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar el llamado?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <form method="POST" id="formEliminarUsuario">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        {{ $llamados->appends([
            'buscar' => request('buscar')
        ])->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function () {

            $("#buscarUsuario").keyup(function(e){
                if(e.keyCode == 13){
                    if ($(this).val() != ''){
                        $("#buscarUsuarioForm").submit();
                    } else{
                        alert("Debe ingresar un parametro de busqueda");
                    }

                }
            });

            $(".eliminar").click(function () {
                var url = $(this).data("url");
                $("#formEliminarUsuario").attr("action", url);
                $("#eliminarUsuario").modal('show');
            });

        });
    </script>
@endsection
