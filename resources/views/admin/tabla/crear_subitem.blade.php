@extends('layouts.master')
@section('titulo', 'Item '. $tabla->nombre)
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <form action="{{ route('llamados.actualizarTribunal', $tabla) }}" method="POST" style="width: 100%" enctype="multipart/form-data">
        @csrf
        <div class="row" id="tarjetero">
            <div class="col-md-6 tarjeta">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Subitem <span class="numero">1</span></h6>
                        <div class="dropdown no-arrow" style="display: none">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item eliminar">Eliminar</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="form-group">
                            <input type="input" class="form-control asignatura_nombre" value="{{ old('asignatura')[1] }}" required name="asignatura[1]" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Letra">
                                </div>
                                <div class="col-md-9">
                                    <input type="number" min="1" class="form-control" placeholder="Puntaje máximo">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="" class="form-control tipo_subitem">
                                        <option disabled selected>Tipo de subitem</option>
                                        <option value="item">Item</option>
                                        <option value="grupo">Grupo</option>
                                        <option value="unico">Unico</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <i class="fas fa-plus-circle fa-2x cursor agregar_campo" style="color: green; display: none"></i>
                                </div>
                            </div>
                        </div>
                        <div id="contenedor_subitem">
                            <div class="form-group subitem" style="display: none">
                                <div class="row">
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" placeholder="Letra" disabled>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="number" class="form-control" required placeholder="Max." disabled>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" required placeholder="Nombre" disabled>
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fas fa-minus-circle fa-2x cursor eliminar_subitem" style="color: red; display: none"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="pb-3 mx-auto">
                <button class="btn btn-danger" id="crear_asignatura">Nueva asignatura</button>
            </div>
        </div>


        <div class="row">
            <div class="mx-auto">
                <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>

@endsection
@section('js')
    <script>
        $(function () {

            $("#crear_asignatura").click(function(e){
                e.preventDefault();
                var cantidad_tarjetas = $(".tarjeta").length;
                var tarjeta = $(".tarjeta:first").clone(true);
                tarjeta.find('.asignatura_nombre')
                    .attr("name", "asignatura["+(++cantidad_tarjetas)+"]")
                    .prop("required", true)
                    .val(null);
                tarjeta.find('.numero')
                    .html(cantidad_tarjetas);
                tarjeta.find('.dropdown')
                    .show();
                $("#tarjetero").append(tarjeta);
            });

            $(document).on("click", ".tipo_subitem", function () {
                var select = $(this).val();

                if ( select == 'grupo' || $(this).val() == 'unico' ){
                    $(this).closest('.row').find('.agregar_campo').show();
                    $(this).closest('.card-body').find('.subitem').show()
                        .find('input').prop('disabled', false);
                    return;
                }
                $(this).closest('.row').find('.agregar_campo').hide();
                $(this).closest('.card-body').find('.subitem').hide()
                    .find('input').prop('disabled', true);
                return;
            });

            $(document).on("click", ".agregar_campo", function(e){
                e.preventDefault();
                var cantidad_subitem = $(".subitem").length;
                var subitem = $(".subitem:first").clone(true);
                subitem.find("input")
                    .attr("name", "")
                    .prop("required", true)
                    .prop("disabled", false)
                    .val(null);

                $("#contenedor_subitem").append(subitem);
            });

            $(document).on("click", ".eliminar", function () {
                $(this).closest(".tarjeta").remove();
            });
        });
    </script>
@endsection
