@extends('layouts.master')
@section('titulo', 'Listado de items')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <div class="row" style="margin-bottom: 1%">
        <div class="col">
            <button href="http://localhost/eie/llamados/nuevo" class="btn btn-primary btn-icon-split"
                    data-toggle="modal" data-target="#crearUsuario">
                    <span class="icon text-white-50">
                        <i class="fas fa-plus"></i>
                    </span>
                <span class="text">Nuevo item</span>
            </button>

        </div>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <th>Letra</th>
            <th>Nombre</th>
            <th>Puntaje máximo</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @forelse($tabla as $t)
                <tr>
                    <td>{{ $t->letra }}</td>
                    <td>{{ $t->nombre }}</td>
                    <td>{{ $t->puntaje_maximo }}</td>
                    <td>
                        <a class="btn btn-success btn-circle btn-sm" title="Crear subitem" href="{{ route('tabla.agregarSubitem', $t->id) }}">
                            <i class="fas fa-plus"></i>
                        </a>

                        <span class="btn btn-warning btn-circle btn-sm editar cursor" title="Editar"
                              data-url="{{route('tabla.editar', $t->id)}}" data-actualizar="{{ route('tabla.actualizar', $t->id) }}">
                            <i class="fas fa-pencil-alt"></i>
                        </span>

                        <span class="btn btn-danger btn-circle btn-sm eliminar cursor" title="Eliminar"
                              data-url="{{route('tabla.eliminar', $t->id)}}">
                            <i class="far fa-trash-alt"></i>
                        </span>
                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        <div class="modal fade bd-example-modal-lg" id="crearUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Crear item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="crearUsuarioForm" action="{{ route('tabla.guardar') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="letra" name="letra" value="{{ old('letra') }}" placeholder="Letra">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="nombre" name="nombre" value="{{ old('name') }}" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" min="1" required id="puntaje_maximo" value="{{ old('puntaje_maximo') }}" name="puntaje_maximo" placeholder="Puntaje máximo ">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="eliminarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar el usuario?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <form method="POST" id="formEliminarUsuario">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="actualizarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="actualizarUsuarioForm" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="letra" name="letra" value="{{ old('letra') }}" placeholder="Letra">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="nombre" name="nombre" value="{{ old('name') }}" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" min="1" required id="puntaje_maximo" value="{{ old('puntaje_maximo') }}" name="puntaje_maximo" placeholder="Puntaje máximo ">
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn-submit-update" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="actualizarPasswordUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar contraseña</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="actualizarPasswordUsuarioForm" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="password" class="form-control" name="password_confirmation" id="password1" placeholder="Repetir contraseña">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn-submit-update-pass" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{ $tabla->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function () {

            function validar_vacio(data, empty){
                if ($(data).val() == '') {
                    $(data).addClass('is-invalid');
                    console.log($(this));
                    empty = true;
                }else{
                    $(data).removeClass('is-invalid');
                }
                return empty
            }
            $("#btn-submit").click(function (e) {
                e.preventDefault();
                var empty = false;
                $('#crearUsuario input').each(function () {
                    empty = validar_vacio($(this), empty);
                });

                if (empty){
                    alert("Debe completar los campos marcados");
                    return;
                }
                $("#crearUsuarioForm").submit();
            });

            $("#buscarUsuario").keyup(function(e){
                if(e.keyCode == 13){
                    if ($(this).val() != ''){
                        $("#buscarUsuarioForm").submit();
                    } else{
                        alert("Debe ingresar un parametro de busqueda");
                    }

                }
            });

            $(".eliminar").click(function () {
                var url = $(this).data("url");
                $("#formEliminarUsuario").attr("action", url);
                $("#eliminarUsuario").modal('show');
            });

            function setDatos(response, url_actualizar){
                $("#actualizarUsuarioForm #letra").val(response.letra);
                $("#actualizarUsuarioForm #nombre").val(response.nombre);
                $("#actualizarUsuarioForm #puntaje_maximo").val(response.puntaje_maximo);
                $("#actualizarUsuarioForm").attr("action", url_actualizar);
                $("#actualizarUsuario").modal("show");
            }

            $("#estado").click(function(){
                $(this).val($(this).is(':checked'));
            });

            $(".editar").click(function () {
                var url = $(this).data("url");
                var url_actualizar = $(this).data("actualizar");
                $.ajax({
                    url: url,
                    dataType: "json",
                }).done(function (response) {
                    setDatos(response, url_actualizar);
                }).fail(function (response) {
                    console.log(response);
                })
            });

            $("#btn-submit-update").click(function (e) {
                e.preventDefault();
                var empty = false;
                $('#actualizarUsuario input').each(function () {
                    empty = validar_vacio($(this), empty);
                });
                if (empty){
                    alert("Debe completar los campos marcados");
                    return;
                }
                $("#actualizarUsuarioForm").submit();
            });

            $(".actualizar-contraseña").click(function () {
                var url_actualizar = $(this).data("actualizar");
                $("#actualizarPasswordUsuarioForm").attr("action", url_actualizar);
                $("#actualizarPasswordUsuario").modal('show');

            });

            $("#btn-submit-update-pass").click(function (e) {
                e.preventDefault();
                var empty = false;
                $("#actualizarPasswordUsuario input").each(function () {
                    empty = validar_vacio($(this), empty);
                });
                if (empty){
                    alert("Debe completar los campos marcados");
                    return;
                }
                var password1 = $("#password").val();
                var password2 = $("#password1").val();
                if (password1 !== password2){
                    alert("Los password no coinciden");
                    $("#password").addClass('is-invalid');
                    $("#password1").addClass('is-invalid');
                    return;
                }
                $("#actualizarPasswordUsuarioForm").submit();
            });
        });
    </script>
@endsection
