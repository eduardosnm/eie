@extends('layouts.master')
@section('titulo', 'Ver postulante')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <style>
        .pdfobject-container { height: 80rem; border: 1rem solid rgba(0,0,0,.1); }

    </style>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Postulante</h6>
                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">Apellido y Nombre</label>
                                <p>{{ $postulacion->postulante->full_name }}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">DNI</label>
                                <p>{{ $postulacion->postulante->dni }}</p>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">Correo electrónico</label>
                                <p>{{ $postulacion->postulante->email }}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">Teléfono</label>
                                <p>{{ $postulacion->postulante->telefono }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Títulos de grado</h6>
                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <ul>
                            @foreach($postulacion->postulante->titulos as $titulo)
                                <li>{{ $titulo }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="h4 mb-0 text-gray-800">Asignaturas en las que se postuló</h3>
    </div>

    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead class="table-dark">
                <tr class="text-center">
                    <th>Asignatura</th>
                    <th>Evaluar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($asignaturas as $asignatura)
                    <tr>
                        <td>{{ $asignatura->nombre }}</td>
                        <td class="text-center">
                            <a href="{{ route('postulaciones.evaluar', [$postulacion, $asignatura]) }}" class="btn btn-sm btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-user-check"></i>
                            </span>
                                <span class="text">Evaluar</span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Curriculum vitae</h6>
                    <div class="dropdown no-arrow">
                        <a href="{{ route('postulaciones.descargarCv', $postulacion->upload) }}" class="btn btn-success btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-download"></i>
                            </span>
                            <span class="text">Descargar</span>
                        </a>

                    </div>
                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <div id="cv">
                            <embed src="{{ action('Admin\PostulacionesController@getCv', ['cv'=> $postulacion->upload->id]) }}"
                                   type="application/pdf" width="100%" height="700rem">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

