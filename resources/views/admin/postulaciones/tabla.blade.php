@extends('layouts.master')
@section('titulo', "Tabla de ponderacion para carga de puntaje")
@section('contenido')

    @include('partials.errors')
    @include('partials.success')

    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Postulante</h6>
                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">Apellido y Nombre</label>
                                <p>{{ $postulacion->postulante->full_name }}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">DNI</label>
                                <p>{{ $postulacion->postulante->dni }}</p>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label class="font-weight-bold">Correo electrónico</label>
                                <p>{{ $postulacion->postulante->email }}</p>
                            </div>

                            <div class="col">
                                <label class="font-weight-bold">Teléfono</label>
                                <p>{{ $postulacion->postulante->telefono }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Títulos de grado</h6>
                </div>
                <div class="card-body">
                    <div class="modal-body">
                        <ul>
                            @foreach($postulacion->postulante->titulos as $titulo)
                                <li>{{ $titulo }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('postulaciones.guardarPuntaje', $asignatura_postulacion) }}"  id="formPuntajes" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">{{ $asignatura->nombre }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="modal-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                @foreach($tabla_ponderacion as $item)
                                    <li class="nav-item">
                                        <a class="nav-link @if ($loop->first) active @endif" id="tab-{{ $item->id }}" data-toggle="tab" href="#{{ $item->letra . "_" . $asignatura->id }}" role="tab" aria-controls="tab{{ $item->id }}" aria-selected="true">{{ $item->nombre }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                @foreach($tabla_ponderacion as $item)
                                    <div class="tab-pane fade show @if ($loop->first) active @endif" id="{{ $item->letra . "_" . $asignatura->id }}" role="tabpanel" aria-labelledby="tab-{{ $item->id }}">
                                        <table class="table table-sm table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Descripción</th>
                                                <th>Punjaje máximo</th>
                                                <th>Puntaje otorgado</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($item->items as $nro => $subItem)
                                                @php $puntuacion = $subItem->asignatura_postulacion()->where('asignatura_postulacion_id', $asignatura_postulacion->id)->first() @endphp
                                                <tr @if( $subItem->hijos->count()) style="font-weight: bold; color:black; background: #f2f2f2"@endif
                                                @if( $subItem->padre ) style="background: #f2f2f2"@endif>
                                                    <td>{{ $subItem->codigo_item }}</td>
                                                    <td>{!! $subItem->nombre_item !!}</td>
                                                    <td>
                                                        {{ $subItem->puntaje_maximo }}
                                                    </td>
                                                    @if( !$subItem->hijos->count())
                                                        <td>
                                                            <input type="number" class="form-control
                                                                @if(empty($subItem->codigo_item)) {{ $subItem->padre->codigo_item."_".$subItem->tipo }} @endif"
                                                                min="{{ $subItem->puntaje_minimo }}" max="{{ $subItem->puntaje_maximo }}" value="{{ $puntuacion->pivot->puntaje ?? '' }}"
                                                                   name="asig_post[{{ $asignatura_postulacion->id }}][subitem][{{ $subItem->id }}]" placeholder="0">
                                                        </td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="mx-auto">
                <button class="btn btn-success btn-lg" type="submit" id="guardar">Guardar</button>
            </div>
        </div>
    </form>
@endsection
@section('js')
    <script>
        $(function () {
            $('input[type=number]').on('wheel', function(e){
                return false;
            });

            // $(".A10_unico").keyup(function () {
            //     if($.trim($(this).val()) == ''){
            //         $(".A10_unico").prop('disabled', false);
            //     }else{
            //         $(".A10_unico").not(this).prop('disabled', true);
            //     }
            // });

            $("input[type=number]").focusout(function(){
                var min = parseInt($(this).attr("min"));
                var max = parseInt($(this).attr("max"));
                var puntaje = parseInt($(this).val());

                if (puntaje < min){
                    alert("El puntaje no puede ser menor a "+min);
                    $(this).val(min);
                    $(this).addClass("is-invalid");
                    $(this).focus();
                }

                if (puntaje > max){
                    alert("El puntaje no puede ser mayor a "+max);
                    $(this).addClass("is-invalid");
                    $(this).val(max);
                    $(this).focus();
                }

                $(this).removeClass("is-invalid").addClass("is-valid");
                return;
            });

            var url = "{{ route('postulaciones.editando', $asignatura_postulacion->id) }}";

            var callback = function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        editando: moment().format("YYYY-MM-DD HH:mm:ss")
                    },
                });
            };

            setInterval(callback, 60000);

            $("form").submit(function(){
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endsection
