@extends('layouts.master')
@section('titulo', 'Listado de postulaciones')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <form action="{{ route('postulaciones.listado') }}" method="GET" id="buscarPostulanteForm">

        <div class="row" style="margin-bottom: 1%">
            <div class="col-md-2">
                <p class="text-danger"><b>Cantidad de resultados: {{ $cantidad_inscriptos }}</b></p>
            </div>

            <div class="col-md-2 offset-3">
                <select name="llamado" id="llamado" class="form-control">
                    <option selected disabled>Seleccionar el llamado</option>
                    <option value="todos">Todos</option>
                    @foreach($llamados as $llamado)
                        <option value="{{ $llamado->id }}" @if(request('llamado') == $llamado->id) selected @endif>{{ $llamado->titulo }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" value="{{ request('postulante') }}" name="postulante" id="buscarPostulante" placeholder="Buscar postulante">
            </div>

            <div class="col-md-2">
                <span class="cursor" title="Buscar" id="buscar" style="color:#3c62d1">
                    <i class="fas fa-search fa-2x"></i>
                </span>
                <a href="{{ route('postulaciones.listado') }}" title="Limpiar resultados" style="color:red">
                    <i class="fas fa-broom fa-2x"></i>
                </a>
            </div>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <th>#</th>
            <th>Postulante</th>
            <th>Llamado</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @forelse($postulaciones as $postulacion)
                <tr>
                    <td>{{ $postulacion->id }}</td>
                    <td>{{ $postulacion->postulante->full_name }}</td>
                    <td>{{ $postulacion->llamado->titulo }}</td>
                    <td>
                        <a href="{{ route('postulaciones.ver', $postulacion) }}" class="btn btn-primary btn-circle btn-sm" title="Detalle">
                            <i class="fas fa-eye"></i>
                        </a>

                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>


        {{ $postulaciones->appends([
            'llamado' => request('llamado'),
            'buscar' => request('postulante')
        ])->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function () {

            $("#llamado").change(function (){
                if ($(this).val() == 'todos'){
                    window.location.href = "{{ route('postulaciones.listado') }}";
                    return;
                }
                $("#buscarPostulanteForm").submit();
            });

            $("#buscarPostulante").keyup(function(e){
                if(e.keyCode == 13){
                    if ($(this).val() != ''){
                        $("#buscarPostulanteForm").submit();
                    } else{
                        alert("Debe ingresar un parametro de busqueda");
                    }

                }
            });

            $("#buscar").click(function () {
                $("#buscarPostulanteForm").submit();
            });

        });
    </script>
@endsection
