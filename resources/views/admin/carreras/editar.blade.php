@extends('layouts.master')
@section('titulo', 'Editar carrera')
@section('contenido')
    @include('partials.errors')
    @include('partials.success')

    <form action="{{ route('carreras.actualizar', $carrera) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-9">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Datos de la carrera</h6>
                    </div>
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="nombre">Nombre de la carrera</label>
                            <input type="input" class="form-control" id="nombre" value="{{ $carrera->nombre }}" required name="nombre" placeholder="Nombre">
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Estado de la carrera</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" @if($carrera->estado) checked @endif class="custom-control-input"
                                       id="estado_checkbox">
                                <label class="custom-control-label" for="estado_checkbox">Estado de la carrera</label>
                                <input type="hidden" id="estado" name="estado" value="{{ $carrera->estado ? 1 : 0 }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="pb-3 mx-auto">
                <button class="btn btn-primary" id="crear_asignatura">Nueva asignatura</button>
            </div>
        </div>

        <div class="row" id="tarjetero">
        @foreach($carrera->asignaturas as $key => $asignatura)
            <div class="col-md-3 tarjeta">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Asignatura <span class="numero">{{ ($key + 1) }}</span></h6>
                        <div class="dropdown no-arrow" @if($loop->first) style="display: none" @endif>
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item eliminar">Eliminar</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="carrera[nombre]">Nombre de la asignatura</label>
                            <input type="input" class="form-control asignatura_nombre" value="{{ $asignatura->nombre  }}" required name="asignatura[{{ ($key+1) }}][nombre]" placeholder="Nombre">
                            <input type="hidden" class="asignatura_id" value="{{ $asignatura->id  }}" required name="asignatura[{{ ($key+1) }}][id]">
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
        </div>

        <div class="row">
            <div class="mx-auto">
                <button class="btn btn-success btn-lg" type="submit">Guardar</button>
            </div>

        </div>
    </form>
    @php
        $asignaturas_old = '';
        if (!is_null(old('asignatura'))){
            $asignaturas_old = old('asignatura');
            unset($asignaturas_old[1]);
        }
    @endphp
@endsection
@section('js')
    <script>
        $(function(){
            $("#crear_asignatura").click(function(e){
                e.preventDefault();
                var cantidad_tarjetas = $(".tarjeta").length;
                var tarjeta = $(".tarjeta:first").clone(true);
                tarjeta.find('.asignatura_nombre')
                    .attr("name", "nuevo["+(++cantidad_tarjetas)+"]")
                    .prop("required", true)
                    .val(null);
                tarjeta.find('.numero')
                    .html(cantidad_tarjetas);
                tarjeta.find('.dropdown')
                    .show();
                $("#tarjetero").append(tarjeta);
            });

            $("#estado_checkbox").change(function () {
               if ($(this).is(":checked")){
                   $("#estado").val(1);
               }else{
                   $("#estado").val(0);
               }
            });

            $(document).on("click", ".eliminar", function () {
                $(this).closest(".tarjeta").remove();
            });

            var asignaturas = JSON.parse('<?php echo json_encode($asignaturas_old) ?>');
            if (asignaturas != ''){
                $.each(asignaturas, function (i, item) {
                    var cantidad_tarjetas = $(".tarjeta").length;
                    var tarjeta = $(".tarjeta:first").clone(true);
                    tarjeta.find('.asignatura_nombre')
                        .attr("name", "nuevo["+(++cantidad_tarjetas)+"]")
                        .prop("required", true)
                        .val(item);
                    tarjeta.find('.numero')
                        .html(cantidad_tarjetas);
                    $("#tarjetero").append(tarjeta);
                })
            }

        });
    </script>
@endsection
