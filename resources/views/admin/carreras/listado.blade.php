@extends('layouts.master')
@section('titulo', 'Listado de carreras')
@section('contenido')
    @include('partials.errors')
    @include('partials.success')
    <div class="row" style="margin-bottom: 1%">
        <div class="col">
            <a href="{{ route('carreras.crear') }}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-plus"></i>
                    </span>
                <span class="text">Nueva carrera</span>
            </a>
        </div>
        <div class="col-md-3">
            <form action="{{ route('carreras.listado') }}" method="GET" id="buscarUsuarioForm">
                <input type="text" class="form-control" value="{{ request('buscar') }}" name="buscar" placeholder="Buscar carrera">
            </form>
        </div>
        <div class="col-md-1">
            <a href="{{ route('carreras.listado') }}" title="Limpiar resultados">
                <i class="fas fa-broom fa-2x"></i>
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <th>#</th>
            <th>Nombre</th>
            <th>Estado</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @forelse($carreras as $carrera)
                <tr>
                    <td>{{ $carrera->id }}</td>
                    <td>{{ $carrera->nombre }}</td>
                    <td>{!! $carrera->estado_label !!}</td>
                    <td>
                        <a href="{{ route('carreras.ver', $carrera) }}" class="btn btn-primary btn-circle btn-sm" title="Detalle">
                            <i class="fas fa-eye"></i>
                        </a>

                        <a href="{{ route('carreras.editar', $carrera) }}" class="btn btn-warning btn-circle btn-sm editar" title="Editar carrera">
                            <i class="far fa-edit"></i>
                        </a>

                        <span class="btn btn-danger btn-circle btn-sm eliminar cursor" title="Eliminar"
                              data-url="{{route('carreras.eliminar', $carrera->id)}}">
                            <i class="far fa-trash-alt"></i>
                        </span>
                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        <div class="modal fade" id="eliminarCarrera" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar la carrera?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4><span style="color: red; font-weight: bold">¡ADVERTENCIA!</span> Si elimina la carrera se eliminaran las asignaturas pertenecientes a esta.</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <form method="POST" id="formEliminarCarrera">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        {{ $carreras->appends([
            'buscar' => request('buscar')
        ])->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            $(".eliminar").click(function () {
                var url = $(this).data("url");
                $("#formEliminarCarrera").attr("action", url);
                $("#eliminarCarrera").modal('show');
            });
        });
    </script>
@endsection
