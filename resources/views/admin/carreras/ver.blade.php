@extends('layouts.master')
@section('titulo', "Detalle de la carrera {$carrera->nombre}"  )
@section('contenido')
<div class="row">

    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Asignaturas</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($carrera->asignaturas->sortBy('nombre') as $asignatura)
                            <tr>
                                <td>{{ $asignatura->nombre }}</td>
                                <td>{!! $asignatura->estado_label !!}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">No se encontraron resultados</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Estado</h6>
            </div>
            <div class="card-body mx-auto">
                <h5>{!! $carrera->estado_label !!}</h5>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Evaluadores</h6>
            </div>
            <div class="card-body mx-auto">
                <ul>
                    @forelse($carrera->evaluadores as $evaluador)
                        <li>{{ $evaluador->full_name }}</li>
                    @empty
                        <li>No se encontraron resultados</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
