<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Verificación de correo electrónico</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset(mix('/css/app.css')) }}" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">


</head>

<body class="bg-gradient-primary">

<div class="container">

    <div class="container">
        <div class="row justify-content-center" style="margin-top: 10%">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Verifique su dirección de correo electrónico') }}</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.') }}
                            </div>
                        @endif

                        {{ __('Antes de continuar, consulte su correo electrónico para ver un enlace de verificación.') }}
                        {{ __('Si no ha recibido el correo electrónico') }}, <a href="{{ route('verification.resend') }}">{{ __('haga clic aquí para solicitar otra.') }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="{{ asset(mix('/js/app.js')) }}"></script>

</body>

</html>
